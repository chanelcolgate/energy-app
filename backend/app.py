from sanic import Sanic
from sanic import response
from sanic.handlers import ContentRangeHandler
from sanic_cors import CORS
from mainapp.api import all_blueprints
import uvicorn
import os

PATH = os.path.abspath(os.path.dirname(__file__))
app = Sanic(__name__)
CORS(app, automation_options=True)
for bp in all_blueprints:
  app.blueprint(bp)

app.static('/static', PATH + '/www/static')
app.static('/file_report', PATH + '/file_report')

# webapp path defined used route decorator
@app.route("/", methods=['GET'])
async def index(request):
  with open(PATH + '/www/index.html', 'r') as content_file:
    content = content_file.read()
  return response.html(content)

@app.route('/<path>', methods=['GET'])
async def index(request, path):
  with open(PATH + '/www/index.html', 'r') as content_file:
    content = content_file.read()
  return response.html(content)

# debug logs enabled with debug = True
if __name__ == "__main__":
  uvicorn.run(
    "app:app",
    host="0.0.0.0",
    port=8081,
    # loop="uvloop",
    workers=1,
    log_level="info",
    debug=True
  )