from mainapp.config import baseUrl
from mainapp.models.m_dashboard import Dashboard

import requests
import pandas as pd
import numpy as np
import calendar

def movecol(df, cols_to_move=[], ref_col='', place='After'):
  cols = df.columns.tolist()
  if place == 'After':
    seg1 = cols[:list(cols).index(ref_col) + 1]
    seg2 = cols_to_move
  if place == 'Before':
    seg1 = cols[:list(cols).index(ref_col)]
    seg2 = cols_to_move + [ref_col]
    
  seg1 = [i for i in seg1 if i not in seg2]
  seg3 = [i for i in cols if i not in seg1 + seg2]
    
  return(df[seg1 + seg2 + seg3])

class Report(Dashboard):
  def __init__(self, token, assetId, assetName, interval, year, month, quarter):
    super().__init__(token, assetId, assetName)
    self.interval = interval
    self.month = int(month)
    self.year = int(year)
    self.quarter = int(quarter)
    self.startMonth = None
    self.endMonth = None
    self.assetNameChart = assetName

  def createReportMonth(self):
    df_company = pd.DataFrame({
      "col": [
        "Công ty TNHH Công nghệ MIND",
        "377 Nguyễn Văn Linh, thành phố Đà Nẵng",
        "0905 559562",
        "info@iotmind.vn"
      ]
    })

    url = f'{baseUrl}/auth/user'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
      "Content-Type": "application/json"
    }
    response = requests.get(url=url, headers=headers)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    df_invoice_to = pd.DataFrame({
      "col": [
        response.json()['firstName'] + ' ' + response.json()['lastName'],
        response.json()['email']
      ]
    })
    periods = calendar.monthrange(self.year, self.month)[1]
    daily_index = pd.date_range(f"{self.year}-{self.month}", periods=periods, freq="D")
    super().getFormulaElectric()
    formula = self.formula
    consumption = None
    consumptionPNO = None
    if self.interval == 'month':
      if formula['method'] != 'byTime':
        super().getMonthConsumption(self.year, self.month)
        consumption = self.consumption
      else:
        super().getMonthConsumptionPNO(self.year, self.month)
        super().getMonthConsumption(self.year, self.month)
        consumptionPNO = self.consumptionPNO
        consumption = self.consumption
    dictData = {}
    for elem in consumption:
      for key in elem:
        if key == 'time':
          continue
        dictData[key] = []
    
    for key in dictData:
      for elem in consumption:
        dictData[key].append(elem[key])
    
    df = pd.DataFrame(dictData, index=daily_index)
    cols = df.columns.to_list()
    df = movecol(df, [cols[0]], cols[-1], place='After')
    nrows, ncols = df.shape
    df_sum = df.resample('M').sum()
    df_total_cost = None
    try:
      if formula['method'] == 'byLevel':
        price = df_sum[self.assetNameChart].to_numpy()
        cost = 0
        if price[0] > 400000:
          cost = (50000*formula['prices'][0]['price'] +
                  50000*formula['prices'][1]['price'] +
                  100000*formula['prices'][2]['price'] +
                  100000*formula['prices'][3]['price'] +
                  100000*formula['prices'][4]['price'] +
                  (price[0]-400000)*formula['prices'][5]['price'])
        elif price[0] > 300000:
          cost = (50000*formula['prices'][0]['price'] +
                  50000*formula['prices'][1]['price'] +
                  100000*formula['prices'][2]['price'] +
                  100000*formula['prices'][3]['price'] +
                  (price[0]-300000)*formula['prices'][4]['price'])
        elif price[0] > 200000:
          cost = (50000*formula['prices'][0]['price'] +
                  50000*formula['prices'][1]['price'] +
                  100000*formula['prices'][2]['price'] +
                  (price[0]-200000)*formula['prices'][3]['price'])
        elif price[0] > 100000:
          cost = (50000*formula['prices'][0]['price'] +
                  50000*formula['prices'][1]['price'] +
                  (price[0]-100000)*formula['prices'][2]['price'])
        elif price[0] > 50000:
          cost = (50000*formula['prices'][0]['price'] +
                  (price[0]-50000)*formula['prices'][1]['price'])
        else:
          cost = price[0]*formula['prices'][0]['price']
        df_sum['Total Cost'] = cost
      elif formula['method'] == 'byTime':
        totalCostData = consumptionPNO
        df_total_cost = pd.DataFrame(totalCostData)
        df_total_cost.drop('time', inplace=True, axis=1)
        for i, row in df_total_cost.iterrows():
          row['peakConsumption'] *= formula['prices'][0]['price']
          row['normalConsumption'] *= formula['prices'][1]['price']
          row['offPeakConsumption'] *= formula['prices'][2]['price']
        df_total_cost = df_total_cost.rename(columns = {
          'peakConsumption': 'Peak',
          'normalConsumption': 'Normal',
          'offPeakConsumption': 'Off-peak'
        })
        df_total_cost = pd.DataFrame(df_total_cost.sum(axis=0))
        row_total_cost = pd.DataFrame([{0: df_total_cost.sum(axis=0)[0], 'index':'Total'}]).set_index('index')
        df_total_cost = pd.concat([df_total_cost, row_total_cost]).reset_index()
      elif formula['method'] == 'fixed':
        price = df_sum[self.assetNameChart].to_numpy()
        df_sum['Total Cost'] = price*formula['prices'][0]['price']
    except TypeError:
      df_total_cost = None
    df_sum = df_sum.T.reset_index()
    with pd.ExcelWriter("./file_report/report_template.xlsx", date_format="dd/mm/yyyy") as writer:
      startrow, startcol = 12, 0
      df.to_excel(writer, header=True, index=True,
                  startrow=startrow, startcol=startcol)
      
      book = writer.book
      sheet = writer.sheets["Sheet1"]
      sheet.set_row(startrow, 24)
      sheet.set_column('A:A', 12)
      sheet.freeze_panes(0, 1)
      sheet.hide_gridlines(2)

      cell_format = book.add_format({
        'align': 'center'
      })
      header_format = book.add_format({
        'align': 'center',
        'border_color': '#FFFFFF',
        'bottom': 1,
        'bg_color': '#EEEEEE'
      })
      header_format.set_text_wrap()

      index_format = book.add_format({
        'num_format': 'dd/mm/yyyy',
        'font_color': '#FFFFFF',
        'bg_color': '#57B223',
        'border_color': '#FFFFFF',
        'bottom': 1,
        'align': 'center'
      })
      data_format = book.add_format({
        'border_color': '#FFFFFF',
        'bottom': 1,
        'bg_color': '#EEEEEE',
        "num_format": "#,##0"
      })
      sheet.set_column(first_col=startcol, last_col=startcol + ncols + 5,
                      width=24, cell_format=cell_format)
      for i, col in enumerate(df.columns):
        sheet.write(startrow, startcol+i+1, col, header_format)
      for i, row in enumerate(df.index):
        sheet.write(startrow+i+1, startcol, row, index_format)
      sheet.conditional_format(
        first_row=startrow+1,
        first_col=startcol+1,
        last_row=startrow + nrows,
        last_col=startcol + ncols,
        options={
          "type": "no_errors",
          "format": data_format
        }
      )
      # Total
      startcol_sum = 4
      startrow_sum = 44
      startcol_total_cost = 4
      startrow_total_cost = 0
      df_sum.to_excel(writer, header=False, index=False,
                      startrow=startrow_sum, startcol=startcol_sum)
      col1_format = book.add_format({
        'align': 'center',
        'border_color': '#57B223',
        'bottom': 1,
        'num_format': '0.00kW'
      })
      col2_format = book.add_format({
        'font_color': '#FFFFFF',
        'bg_color': '#57B223',
      })
      col3_format = book.add_format({
        'align': 'center',
        'border_color': '#57B223',
        'bottom': 1,
        'top': 1,
        'right': 1,
        'left': 1,
        "num_format": "#,##0đ"
      })
      nrows_df_sum, ncols_df_sum = df_sum.shape
      for i, row in df_sum.iterrows():
        startrow_total_cost = startrow_sum + i + 1
        sheet.write(startrow_sum+i, startcol_sum+1, row[1]/1000, col1_format)
      if df_sum['index'][nrows_df_sum - 1] == 'Total Cost':
        cell_total_format = book.add_format({
          'bg_color': '#57B223',
          'font_color': '#FFFFFF',
        })
        sheet.conditional_format(first_row=startrow_sum+1, first_col=startcol+1,
                                 last_row=startrow_sum+nrows_df_sum, last_col=startcol_sum+ncols_df_sum,
                                 options={
                                  "type": 'text',
                                  "criteria": 'containing',
                                  'value': 'Total',
                                  "format": cell_total_format
                                })
        sheet.write(startrow_sum + nrows_df_sum - 1, startcol_sum + 1,
                    df_sum.loc[nrows_df_sum-1, :][1]/1000, col3_format)
      if isinstance(df_total_cost, pd.DataFrame):
        df_total_cost.to_excel(writer, header=False, index=False,
                               startrow=startrow_total_cost, startcol=startcol_total_cost)
        for i, row in df_total_cost.iterrows():
          sheet.write(startrow_total_cost+i, startcol_total_cost, row['index'], col2_format)
          sheet.write(startrow_total_cost+i, startcol_total_cost+1, row[0]/1000, col3_format)
    
      if len(df.columns) > 1:
        # Chart Column 1
        chart_column_1 = book.add_chart({"type": "column", "subtype": "stacked"})
        chart_column_1.set_title({"name": f"Energy Consumed Per Day {self.assetNameChart}"})
        chart_column_1.set_size({"width": 856, "height": 312})
    
        # Add each column as a series, ignoring total row and col
        for col in range(1, ncols):
          chart_column_1.add_series({
            "name": ["Sheet1", startrow, startcol + col],
            "categories": ["Sheet1", startrow + 1, startcol,
                            startrow + nrows, startcol],
            "values": ["Sheet1", startrow + 1, startcol + col,
                        startrow + nrows, startcol + col],
          })
        
        # Chart formatting
        chart_column_1.set_x_axis({
          "name": df.index.name,
          "major_tick_mark": "none",
          "num_format": "dd/mm"
        })
        chart_column_1.set_y_axis({
          "name": "Energy Consumed",
          "line": {"none": True},
          "major_gridlines": {"visible": True},
          "major_tick_mark": "none",
          "num_format": "#,##0W"
        })
    
        chart_column_1.set_style(13)
        chart_column_1.set_legend({'none': True})
        # Add the chart to the sheet
        sheet.insert_chart(startrow_total_cost + 5, startcol + 1, chart_column_1)
        
        # Chart Pie 1
        chart_pie_1 = book.add_chart({"type": "pie"})
        chart_pie_1.set_title({"name": f"Energy Consumed Of {self.assetNameChart}"})
        chart_pie_1.set_size({"width": 312, "height": 312})
        values = []
        categories = []
        for col in range(0, ncols-1):
          values.append('F' + str(startrow_sum + 1 +  col))
          categories.append('E' + str(startrow_sum + 1 +  col))
        chart_pie_1.add_series({
          'categories': '=Sheet1!'+','.join(categories),
          'values':     '=Sheet1!'+','.join(values),
        })
        # Set an Excel chart style. Colors with white outline and shadow.
        chart_pie_1.set_style(13)
        chart_pie_1.set_legend({'none': False})

        # Insert the chart into the worksheet (with an offset).
        sheet.insert_chart(startrow_total_cost + 22, startcol + 1, chart_pie_1)
    
        # Chart Pie 2
        if isinstance(df_total_cost, pd.DataFrame):
          chart_pie_2 = book.add_chart({"type": "pie"})
          chart_pie_2.set_title({"name": "Usage"})
          chart_pie_2.set_size({"width": 321, "height": 312})
          values = []
          categories = []
          for col in range(0, 3):
            values.append('F' + str(startrow_total_cost + 1 + col))
            categories.append('E' + str(startrow_total_cost + 1 + col))
          chart_pie_2.add_series({
            'categories': '=Sheet1!'+','.join(categories),
            'values': '=Sheet1!'+','.join(values)
          })
          chart_pie_2.set_style(14)
          sheet.insert_chart(startrow_total_cost + 22, startcol + 4, chart_pie_2)
      else:
        chart_column_1 = book.add_chart({"type": "column", "subtype": "stacked"})
        chart_column_1.set_title({"name": f"Energy Consumed Per Day {self.assetNameChart}"})
        chart_column_1.set_size({"width": 966, "height": 312})
    
        # Add each column as a series, ignoring total row and col
        chart_column_1.add_series({
          "name": ["Sheet1", startrow, startcol],
          "categories": '=Sheet1!A14:A44', 
          "values": '=Sheet1!B14:B44',
        })
        
        # Chart formatting
        chart_column_1.set_x_axis({
            "name": df.index.name,
            "major_tick_mark": "none",
            "num_format": "dd/mm"
        })
        chart_column_1.set_y_axis({
            "name": "Energy Consumed",
            "line": {"none": True},
            "major_gridlines": {"visible": True},
            "major_tick_mark": "none",
            "num_format": "#,##0W"
        })
    
        chart_column_1.set_style(13)
        chart_column_1.set_legend({'none': True})
        # Add the chart to the sheet
        sheet.insert_chart(startrow_total_cost + 5, startcol + 1, chart_column_1)

      # Day la phan header   
      sheet.insert_image("B1", "logo.png")
    
      # Day la phan Company
      startrow, startcol = 0, 5
      company_format = book.add_format({
          'align': 'right',
      })
      company_name_format = book.add_format({
          'bold': True,
      })
      email_format = book.add_format({
          'color': '#57B223'
      })
      split_format = book.add_format({
          'bottom': 1,
          'border_color': "#000000"
      })
      for i, row in df_company.iterrows():
          sheet.write(startrow+i, startcol, row['col'], company_format)
      sheet.conditional_format('F1', {
          'type': 'no_errors',
          'format': company_name_format
      })
      sheet.conditional_format('F4', {
          'type': 'no_errors',
          'format': email_format
      })
      sheet.conditional_format('B6:F6', {
          'type': 'no_errors',
          'format': split_format
      })
    
      b8_format = book.add_format({
          'left': 5,
          'border_color': '#57B223',
      })
      sheet.write("B8", "    REPORT TO:", b8_format)
    
      startrow, startcol = 8, 1
      for i, row in df_invoice_to.iterrows():
          sheet.write(startrow+i, startcol, f"    {row['col']}", b8_format)
    
      title_format = book.add_format({
          'align': 'right',
          'size': 24,
          'bold': True,
          'color': '#57B223'
      })
      sheet.write("F8", f"REPORT MONTH {self.month}/{self.year}", title_format)
      sheet.set_row(7, 24)
      date_format_start = book.add_format({
          "num_format": "dd/mm/yyyy",
          'align': 'right'
      })
      date_format_end = book.add_format({
          "num_format": "dd/mm/yyyy",
          'align': 'right'
      })
      sheet.write('F9', pd.Timestamp(f"{self.year}-{self.month}-01"), date_format_start)
      sheet.write('F10', pd.date_range(f"{self.year}-{self.month}-01", periods=1, freq="M")[0], date_format_start)
    return {
      'status_code': response.status_code
    }
  
  def createReportQuarter(self):
    df_company = pd.DataFrame({
      "col": [
        "Công ty TNHH Công nghệ MIND",
        "377 Nguyễn Văn Linh, thành phố Đà Nẵng",
        "0905 559562",
        "info@iotmind.vn"
      ]
    })

    url = f'{baseUrl}/auth/user'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
      "Content-Type": "application/json"
    }
    response = requests.get(url=url, headers=headers)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    df_invoice_to = pd.DataFrame({
      "col": [
        response.json()['firstName'] + ' ' + response.json()['lastName'],
        response.json()['email']
      ]
    })
    super().getFormulaElectric()
    formula = self.formula
    consumption = None
    consumptionPNO = None
    if formula['method'] != 'byTime':
      super().getYearConsumption(self.year, self.quarter)
      consumption = self.consumption
    else:
      super().getYearConsumptionPNO(self.year, self.quarter)
      consumptionPNO = self.consumptionPNO
      super().getYearConsumption(self.year, self.quarter)
      consumption = self.consumption
    dictData = {}
    time = []
    for elem in consumption:
      for key in elem:
        if key == 'time':
          time.append(elem[key])
          continue
        dictData[key] = []

    for key in dictData:
      for elem in consumption:
        dictData[key].append(elem[key])
    
    monthly_index = pd.Index(time)
    df = pd.DataFrame(dictData, index=monthly_index)
    df.index = pd.to_datetime(df.index)
    cols = df.columns.to_list()
    df = movecol(df, [cols[0]], cols[-1], place='After')
    nrows, ncols = df.shape
    df_sum = df.resample('Y').sum()
    df_total_cost = None
    try:
      if formula['method'] == 'byLevel':
        costs = []
        for price in df[self.assetNameChart]:
          cost = 0
          if price > 400000:
            cost = (50000*formula['prices'][0]['price'] +
                    50000*formula['prices'][1]['price'] +
                    100000*formula['prices'][2]['price'] +
                    100000*formula['prices'][3]['price'] +
                    100000*formula['prices'][4]['price'] +
                    (price-400000)*formula['prices'][5]['price'])
          elif price > 300000:
            cost = (50000*formula['prices'][0]['price'] +
                    50000*formula['prices'][1]['price'] +
                    100000*formula['prices'][2]['price'] +
                    100000*formula['prices'][3]['price'] +
                    (price-300000)*formula['prices'][4]['price'])
          elif price > 200000:
            cost = (50000*formula['prices'][0]['price'] +
                    50000*formula['prices'][1]['price'] +
                    100000*formula['prices'][2]['price'] +
                    (price-200000)*formula['prices'][3]['price'])
          elif price > 100000:
            cost = (50000*formula['prices'][0]['price'] +
                    50000*formula['prices'][1]['price'] +
                    (price-100000)*formula['prices'][2]['price'])
          elif price > 50000:
            cost = (50000*formula['prices'][0]['price'] +
                    (price-50000)*formula['prices'][1]['price'])
          else:
            cost = price*formula['prices'][0]['price']
          costs.append(cost)
        df_sum['Total Cost'] = np.nansum(costs)
      elif formula['method'] == 'byTime':
        totalCostData = consumptionPNO
        df_total_cost = pd.DataFrame(totalCostData)
        df_total_cost.drop('time', inplace=True, axis=1)
        for i, row in df_total_cost.iterrows():
          row['peakConsumption'] *= formula['prices'][0]['price']
          row['normalConsumption'] *= formula['prices'][1]['price']
          row['offPeakConsumption'] *= formula['prices'][2]['price']
        df_total_cost = df_total_cost.rename(columns = {
          'peakConsumption': 'Peak',
          'normalConsumption': 'Normal',
          'offPeakConsumption': 'Off-peak'
        })
        df_total_cost = pd.DataFrame(df_total_cost.sum(axis=0))
        row_total_cost = pd.DataFrame([{0: df_total_cost.sum(axis=0)[0], 'index':'Total'}]).set_index('index')
        df_total_cost = pd.concat([df_total_cost, row_total_cost]).reset_index()
      elif formula['method'] == 'fixed':
        price = df_sum[self.assetNameChart].to_numpy()
        df_sum['Total Cost'] = price*formula['prices'][0]['price']
    except TypeError:
      df_total_cost = None
    df_sum = df_sum.T.reset_index()
    with pd.ExcelWriter("./file_report/report_template.xlsx", date_format="dd/mm/yyyy") as writer:
      startrow, startcol = 12, 0
      df.to_excel(writer, header=True, index=True,
                  startrow=startrow, startcol=startcol)
      
      book = writer.book
      sheet = writer.sheets["Sheet1"]
      sheet.set_row(startrow, 24)
      sheet.set_column('A:A', 12)
      sheet.freeze_panes(0, 1)
      sheet.hide_gridlines(2)

      cell_format = book.add_format({
        'align': 'center'
      })
      header_format = book.add_format({
        'align': 'center',
        'border_color': '#FFFFFF',
        'bottom': 1,
        'bg_color': '#EEEEEE'
      })
      header_format.set_text_wrap()

      index_format = book.add_format({
        'num_format': 'dd/mm/yyyy',
        'font_color': '#FFFFFF',
        'bg_color': '#57B223',
        'border_color': '#FFFFFF',
        'bottom': 1,
        'align': 'center'
      })
      data_format = book.add_format({
        'border_color': '#FFFFFF',
        'bottom': 1,
        'bg_color': '#EEEEEE',
        "num_format": "#,##0"
      })
      sheet.set_column(first_col=startcol, last_col=startcol + ncols + 5,
                      width=24, cell_format=cell_format)
      for i, col in enumerate(df.columns):
        sheet.write(startrow, startcol+i+1, col, header_format)
      for i, row in enumerate(df.index):
        sheet.write(startrow+i+1, startcol, row, index_format)
      sheet.conditional_format(
        first_row=startrow+1,
        first_col=startcol+1,
        last_row=startrow + nrows,
        last_col=startcol + ncols,
        options={
          "type": "no_errors",
          "format": data_format
        }
      )
      # Total
      startcol_sum = 4
      startrow_sum = 16
      startcol_total_cost = 4
      startrow_total_cost = 0
      df_sum.to_excel(writer, header=False, index=False,
                      startrow=startrow_sum, startcol=startcol_sum)
      col1_format = book.add_format({
        'align': 'center',
        'border_color': '#57B223',
        'bottom': 1,
        'num_format': '0.00kW'
      })
      col2_format = book.add_format({
        'font_color': '#FFFFFF',
        'bg_color': '#57B223',
      })
      col3_format = book.add_format({
        'align': 'center',
        'border_color': '#57B223',
        'bottom': 1,
        'top': 1,
        'right': 1,
        'left': 1,
        "num_format": "#,##0đ"
      })
      nrows_df_sum, ncols_df_sum = df_sum.shape
      for i, row in df_sum.iterrows():
        startrow_total_cost = startrow_sum + i + 1
        sheet.write(startrow_sum+i, startcol_sum+1, row[1]/1000, col1_format)
      if df_sum['index'][nrows_df_sum - 1] == 'Total Cost':
        cell_total_format = book.add_format({
          'bg_color': '#57B223',
          'font_color': '#FFFFFF',
        })
        sheet.conditional_format(first_row=startrow_sum+1, first_col=startcol+1,
                                 last_row=startrow_sum+nrows_df_sum, last_col=startcol_sum+ncols_df_sum,
                                 options={
                                  "type": 'text',
                                  "criteria": 'containing',
                                  'value': 'Total',
                                  "format": cell_total_format
                                })
        sheet.write(startrow_sum + nrows_df_sum - 1, startcol_sum + 1,
                    df_sum.loc[nrows_df_sum-1, :][1]/1000, col3_format)
      if isinstance(df_total_cost, pd.DataFrame):
        df_total_cost.to_excel(writer, header=False, index=False,
                               startrow=startrow_total_cost, startcol=startcol_total_cost)
        for i, row in df_total_cost.iterrows():
          sheet.write(startrow_total_cost+i, startcol_total_cost, row['index'], col2_format)
          sheet.write(startrow_total_cost+i, startcol_total_cost+1, row[0]/1000, col3_format)
    
      if len(df.columns) > 1:
        # Chart Column 1
        chart_column_1 = book.add_chart({"type": "column", "subtype": "stacked"})
        chart_column_1.set_title({"name": f"Energy Consumed Per Month {self.assetNameChart}"})
        chart_column_1.set_size({"width": 856, "height": 312})
    
        # Add each column as a series, ignoring total row and col
        for col in range(1, ncols):
          chart_column_1.add_series({
            "name": ["Sheet1", startrow, startcol + col],
            "categories": ["Sheet1", startrow + 1, startcol,
                            startrow + nrows, startcol],
            "values": ["Sheet1", startrow + 1, startcol + col,
                        startrow + nrows, startcol + col],
          })
        
        # Chart formatting
        chart_column_1.set_x_axis({
          "name": df.index.name,
          "major_tick_mark": "none",
          "num_format": "dd/mm"
        })
        chart_column_1.set_y_axis({
          "name": "Energy Consumed",
          "line": {"none": True},
          "major_gridlines": {"visible": True},
          "major_tick_mark": "none",
          "num_format": "#,##0W"
        })
    
        chart_column_1.set_style(13)
        chart_column_1.set_legend({'none': True})
        # Add the chart to the sheet
        sheet.insert_chart(startrow_total_cost + 5, startcol + 1, chart_column_1)
        
        # Chart Pie 1
        chart_pie_1 = book.add_chart({"type": "pie"})
        chart_pie_1.set_title({"name": f"Energy Consumed Of {self.assetNameChart}"})
        chart_pie_1.set_size({"width": 312, "height": 312})
        values = []
        categories = []
        for col in range(0, ncols-1):
          values.append('F' + str(startrow_sum + 1 +  col))
          categories.append('E' + str(startrow_sum + 1 +  col))
        chart_pie_1.add_series({
          'categories': '=Sheet1!'+','.join(categories),
          'values':     '=Sheet1!'+','.join(values),
        })
        # Set an Excel chart style. Colors with white outline and shadow.
        chart_pie_1.set_style(13)
        chart_pie_1.set_legend({'none': False})

        # Insert the chart into the worksheet (with an offset).
        sheet.insert_chart(startrow_total_cost + 22, startcol + 1, chart_pie_1)
    
        # Chart Pie 2
        if isinstance(df_total_cost, pd.DataFrame):
          chart_pie_2 = book.add_chart({"type": "pie"})
          chart_pie_2.set_title({"name": "Usage"})
          chart_pie_2.set_size({"width": 321, "height": 312})
          values = []
          categories = []
          for col in range(0, 3):
            values.append('F' + str(startrow_total_cost + 1 + col))
            categories.append('E' + str(startrow_total_cost + 1 + col))
          chart_pie_2.add_series({
            'categories': '=Sheet1!'+','.join(categories),
            'values': '=Sheet1!'+','.join(values)
          })
          chart_pie_2.set_style(14)
          sheet.insert_chart(startrow_total_cost + 22, startcol + 4, chart_pie_2)
      else:
        chart_column_1 = book.add_chart({"type": "column", "subtype": "stacked"})
        chart_column_1.set_title({"name": f"Energy Consumed Per Day {self.assetNameChart}"})
        chart_column_1.set_size({"width": 966, "height": 312})
    
        # Add each column as a series, ignoring total row and col
        chart_column_1.add_series({
          "name": ["Sheet1", startrow, startcol],
          "categories": '=Sheet1!A14:A44', 
          "values": '=Sheet1!B14:B44',
        })
        
        # Chart formatting
        chart_column_1.set_x_axis({
            "name": df.index.name,
            "major_tick_mark": "none",
            "num_format": "dd/mm"
        })
        chart_column_1.set_y_axis({
            "name": "Energy Consumed",
            "line": {"none": True},
            "major_gridlines": {"visible": True},
            "major_tick_mark": "none",
            "num_format": "#,##0W"
        })
    
        chart_column_1.set_style(13)
        chart_column_1.set_legend({'none': True})
        # Add the chart to the sheet
        sheet.insert_chart(startrow_total_cost + 5, startcol + 1, chart_column_1)

      # Day la phan header   
      sheet.insert_image("B1", "logo.png")
    
      # Day la phan Company
      startrow, startcol = 0, 5
      company_format = book.add_format({
          'align': 'right',
      })
      company_name_format = book.add_format({
          'bold': True,
      })
      email_format = book.add_format({
          'color': '#57B223'
      })
      split_format = book.add_format({
          'bottom': 1,
          'border_color': "#000000"
      })
      for i, row in df_company.iterrows():
          sheet.write(startrow+i, startcol, row['col'], company_format)
      sheet.conditional_format('F1', {
          'type': 'no_errors',
          'format': company_name_format
      })
      sheet.conditional_format('F4', {
          'type': 'no_errors',
          'format': email_format
      })
      sheet.conditional_format('B6:F6', {
          'type': 'no_errors',
          'format': split_format
      })
    
      b8_format = book.add_format({
          'left': 5,
          'border_color': '#57B223',
      })
      sheet.write("B8", "    REPORT TO:", b8_format)
    
      startrow, startcol = 8, 1
      for i, row in df_invoice_to.iterrows():
          sheet.write(startrow+i, startcol, f"    {row['col']}", b8_format)
    
      title_format = book.add_format({
          'align': 'right',
          'size': 24,
          'bold': True,
          'color': '#57B223'
      })
      if self.quarter == 1:
        self.startMonth = 1 
        self.endMonth = 3
      elif self.quarter == 2:
        self.startMonth = 4
        self.endMonth = 6
      elif self.quarter == 3:
        self.startMonth = 7
        self.endMonth = 9
      elif self.quarter == 4:
        self.startMonth = 10
        self.endMonth = 12
      sheet.write("F8", f"REPORT QUARTER {self.quarter}/{self.year}", title_format)
      sheet.set_row(7, 24)
      date_format_start = book.add_format({
          "num_format": "dd/mm/yyyy",
          'align': 'right'
      })
      sheet.write('F9', pd.Timestamp(f"{self.year}-{self.startMonth}"), date_format_start)
      sheet.write('F10', pd.date_range(f"{self.year}-{self.endMonth}", periods=1, freq="M")[0], date_format_start)

    return {
      'status_code': response.status_code
    }
  
  def createReportYear(self):
    df_company = pd.DataFrame({
      "col": [
        "Công ty TNHH Công nghệ MIND",
        "377 Nguyễn Văn Linh, thành phố Đà Nẵng",
        "0905 559562",
        "info@iotmind.vn"
      ]
    })

    url = f'{baseUrl}/auth/user'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
      "Content-Type": "application/json"
    }
    response = requests.get(url=url, headers=headers)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    df_invoice_to = pd.DataFrame({
      "col": [
        response.json()['firstName'] + ' ' + response.json()['lastName'],
        response.json()['email']
      ]
    })
    super().getFormulaElectric()
    formula = self.formula
    consumption = None
    consumptionPNO = None
    if formula['method'] != 'byTime':
      super().getYearConsumption(self.year, self.quarter)
      consumption = self.consumption
    else:
      super().getYearConsumptionPNO(self.year, self.quarter)
      consumptionPNO = self.consumptionPNO
      super().getYearConsumption(self.year, self.quarter)
      consumption = self.consumption
    df = pd.DataFrame(consumption).set_index('time')
    df.index = pd.to_datetime(df.index)
    df.index.name = ''
    cols = df.columns.to_list()
    df = movecol(df, [cols[0]], cols[-1], place='After')
    nrows, ncols = df.shape
    df_sum = df.resample('Y').sum()
    df_total_cost = None
    try:
      if formula['method'] == 'byLevel':
        costs = []
        for price in df[self.assetNameChart]:
          cost = 0
          if price > 400000:
            cost = (50000*formula['prices'][0]['price'] +
                    50000*formula['prices'][1]['price'] +
                    100000*formula['prices'][2]['price'] +
                    100000*formula['prices'][3]['price'] +
                    100000*formula['prices'][4]['price'] +
                    (price-400000)*formula['prices'][5]['price'])
          elif price > 300000:
            cost = (50000*formula['prices'][0]['price'] +
                    50000*formula['prices'][1]['price'] +
                    100000*formula['prices'][2]['price'] +
                    100000*formula['prices'][3]['price'] +
                    (price-300000)*formula['prices'][4]['price'])
          elif price > 200000:
            cost = (50000*formula['prices'][0]['price'] +
                    50000*formula['prices'][1]['price'] +
                    100000*formula['prices'][2]['price'] +
                    (price-200000)*formula['prices'][3]['price'])
          elif price > 100000:
            cost = (50000*formula['prices'][0]['price'] +
                    50000*formula['prices'][1]['price'] +
                    (price-100000)*formula['prices'][2]['price'])
          elif price > 50000:
            cost = (50000*formula['prices'][0]['price'] +
                    (price-50000)*formula['prices'][1]['price'])
          else:
            cost = price*formula['prices'][0]['price']
          costs.append(cost)
        df_sum['Total Cost'] = np.nansum(costs)
      elif formula['method'] == 'byTime':
        totalCostData = consumptionPNO
        df_total_cost = pd.DataFrame(totalCostData)
        df_total_cost.drop('time', inplace=True, axis=1)
        for i, row in df_total_cost.iterrows():
          row['peakConsumption'] *= formula['prices'][0]['price']
          row['normalConsumption'] *= formula['prices'][1]['price']
          row['offPeakConsumption'] *= formula['prices'][2]['price']
        df_total_cost = df_total_cost.rename(columns = {
          'peakConsumption': 'Peak',
          'normalConsumption': 'Normal',
          'offPeakConsumption': 'Off-peak'
        })
        df_total_cost = pd.DataFrame(df_total_cost.sum(axis=0))
        row_total_cost = pd.DataFrame([{0: df_total_cost.sum(axis=0)[0], 'index':'Total'}]).set_index('index')
        df_total_cost = pd.concat([df_total_cost, row_total_cost]).reset_index()
      elif formula['method'] == 'fixed':
        price = df_sum[self.assetNameChart].to_numpy()
        df_sum['Total Cost'] = price*formula['prices'][0]['price']
    except TypeError:
      df_total_cost = None
    df_sum = df_sum.T.reset_index()
    with pd.ExcelWriter("./file_report/report_template.xlsx", date_format="dd/mm/yyyy") as writer:
      startrow, startcol = 12, 0
      df.to_excel(writer, header=True, index=True,
                  startrow=startrow, startcol=startcol)
      
      book = writer.book
      sheet = writer.sheets["Sheet1"]
      sheet.set_row(startrow, 24)
      sheet.set_column('A:A', 12)
      sheet.freeze_panes(0, 1)
      sheet.hide_gridlines(2)

      cell_format = book.add_format({
        'align': 'center'
      })
      header_format = book.add_format({
        'align': 'center',
        'border_color': '#FFFFFF',
        'bottom': 1,
        'bg_color': '#EEEEEE'
      })
      header_format.set_text_wrap()

      index_format = book.add_format({
        'num_format': 'dd/mm/yyyy',
        'font_color': '#FFFFFF',
        'bg_color': '#57B223',
        'border_color': '#FFFFFF',
        'bottom': 1,
        'align': 'center'
      })
      data_format = book.add_format({
        'border_color': '#FFFFFF',
        'bottom': 1,
        'bg_color': '#EEEEEE',
        "num_format": "#,##0"
      })
      sheet.set_column(first_col=startcol, last_col=startcol + ncols + 5,
                      width=24, cell_format=cell_format)
      for i, col in enumerate(df.columns):
        sheet.write(startrow, startcol+i+1, col, header_format)
      for i, row in enumerate(df.index):
        sheet.write(startrow+i+1, startcol, row, index_format)
      sheet.conditional_format(
        first_row=startrow+1,
        first_col=startcol+1,
        last_row=startrow + nrows,
        last_col=startcol + ncols,
        options={
          "type": "no_errors",
          "format": data_format
        }
      )
      # Total
      startcol_sum = 4
      startrow_sum = 25 
      startcol_total_cost = 4
      startrow_total_cost = 0
      df_sum.to_excel(writer, header=False, index=False,
                      startrow=startrow_sum, startcol=startcol_sum)
      col1_format = book.add_format({
        'align': 'center',
        'border_color': '#57B223',
        'bottom': 1,
        'num_format': '0.00kW'
      })
      col2_format = book.add_format({
        'font_color': '#FFFFFF',
        'bg_color': '#57B223',
      })
      col3_format = book.add_format({
        'align': 'center',
        'border_color': '#57B223',
        'bottom': 1,
        'top': 1,
        'right': 1,
        'left': 1,
        "num_format": "#,##0đ"
      })
      nrows_df_sum, ncols_df_sum = df_sum.shape
      for i, row in df_sum.iterrows():
        startrow_total_cost = startrow_sum + i + 1
        sheet.write(startrow_sum+i, startcol_sum+1, row[1]/1000, col1_format)
      if df_sum['index'][nrows_df_sum - 1] == 'Total Cost':
        cell_total_format = book.add_format({
          'bg_color': '#57B223',
          'font_color': '#FFFFFF',
        })
        sheet.conditional_format(first_row=startrow_sum+1, first_col=startcol+1,
                                 last_row=startrow_sum+nrows_df_sum, last_col=startcol_sum+ncols_df_sum,
                                 options={
                                  "type": 'text',
                                  "criteria": 'containing',
                                  'value': 'Total',
                                  "format": cell_total_format
                                })
        sheet.write(startrow_sum + nrows_df_sum - 1, startcol_sum + 1,
                    df_sum.loc[nrows_df_sum-1, :][1]/1000, col3_format)
      if isinstance(df_total_cost, pd.DataFrame):
        df_total_cost.to_excel(writer, header=False, index=False,
                               startrow=startrow_total_cost, startcol=startcol_total_cost)
        for i, row in df_total_cost.iterrows():
          sheet.write(startrow_total_cost+i, startcol_total_cost, row['index'], col2_format)
          sheet.write(startrow_total_cost+i, startcol_total_cost+1, row[0]/1000, col3_format)
    
      if len(df.columns) > 1:
        # Chart Column 1
        chart_column_1 = book.add_chart({"type": "column", "subtype": "stacked"})
        chart_column_1.set_title({"name": f"Energy Consumed Per Month {self.assetNameChart}"})
        chart_column_1.set_size({"width": 856, "height": 312})
    
        # Add each column as a series, ignoring total row and col
        for col in range(1, ncols):
          chart_column_1.add_series({
            "name": ["Sheet1", startrow, startcol + col],
            "categories": ["Sheet1", startrow + 1, startcol,
                            startrow + nrows, startcol],
            "values": ["Sheet1", startrow + 1, startcol + col,
                        startrow + nrows, startcol + col],
          })
        
        # Chart formatting
        chart_column_1.set_x_axis({
          "name": df.index.name,
          "major_tick_mark": "none",
          "num_format": "dd/mm"
        })
        chart_column_1.set_y_axis({
          "name": "Energy Consumed",
          "line": {"none": True},
          "major_gridlines": {"visible": True},
          "major_tick_mark": "none",
          "num_format": "#,##0W"
        })
    
        chart_column_1.set_style(13)
        chart_column_1.set_legend({'none': True})
        # Add the chart to the sheet
        sheet.insert_chart(startrow_total_cost + 5, startcol + 1, chart_column_1)
        
        # Chart Pie 1
        chart_pie_1 = book.add_chart({"type": "pie"})
        chart_pie_1.set_title({"name": f"Energy Consumed Of {self.assetNameChart}"})
        chart_pie_1.set_size({"width": 312, "height": 312})
        values = []
        categories = []
        for col in range(0, ncols-1):
          values.append('F' + str(startrow_sum + 1 +  col))
          categories.append('E' + str(startrow_sum + 1 +  col))
        chart_pie_1.add_series({
          'categories': '=Sheet1!'+','.join(categories),
          'values':     '=Sheet1!'+','.join(values),
        })
        # Set an Excel chart style. Colors with white outline and shadow.
        chart_pie_1.set_style(13)
        chart_pie_1.set_legend({'none': False})

        # Insert the chart into the worksheet (with an offset).
        sheet.insert_chart(startrow_total_cost + 22, startcol + 1, chart_pie_1)
    
        # Chart Pie 2
        if isinstance(df_total_cost, pd.DataFrame):
          chart_pie_2 = book.add_chart({"type": "pie"})
          chart_pie_2.set_title({"name": "Usage"})
          chart_pie_2.set_size({"width": 321, "height": 312})
          values = []
          categories = []
          for col in range(0, 3):
            values.append('F' + str(startrow_total_cost + 1 + col))
            categories.append('E' + str(startrow_total_cost + 1 + col))
          chart_pie_2.add_series({
            'categories': '=Sheet1!'+','.join(categories),
            'values': '=Sheet1!'+','.join(values)
          })
          chart_pie_2.set_style(14)
          sheet.insert_chart(startrow_total_cost + 22, startcol + 4, chart_pie_2)
      else:
        chart_column_1 = book.add_chart({"type": "column", "subtype": "stacked"})
        chart_column_1.set_title({"name": f"Energy Consumed Per Day {self.assetNameChart}"})
        chart_column_1.set_size({"width": 966, "height": 312})
    
        # Add each column as a series, ignoring total row and col
        chart_column_1.add_series({
          "name": ["Sheet1", startrow, startcol],
          "categories": '=Sheet1!A14:A44', 
          "values": '=Sheet1!B14:B44',
        })
        
        # Chart formatting
        chart_column_1.set_x_axis({
            "name": df.index.name,
            "major_tick_mark": "none",
            "num_format": "dd/mm"
        })
        chart_column_1.set_y_axis({
            "name": "Energy Consumed",
            "line": {"none": True},
            "major_gridlines": {"visible": True},
            "major_tick_mark": "none",
            "num_format": "#,##0W"
        })
    
        chart_column_1.set_style(13)
        chart_column_1.set_legend({'none': True})
        # Add the chart to the sheet
        sheet.insert_chart(startrow_total_cost + 5, startcol + 1, chart_column_1)

      # Day la phan header   
      sheet.insert_image("B1", "logo.png")
    
      # Day la phan Company
      startrow, startcol = 0, 5
      company_format = book.add_format({
          'align': 'right',
      })
      company_name_format = book.add_format({
          'bold': True,
      })
      email_format = book.add_format({
          'color': '#57B223'
      })
      split_format = book.add_format({
          'bottom': 1,
          'border_color': "#000000"
      })
      for i, row in df_company.iterrows():
          sheet.write(startrow+i, startcol, row['col'], company_format)
      sheet.conditional_format('F1', {
          'type': 'no_errors',
          'format': company_name_format
      })
      sheet.conditional_format('F4', {
          'type': 'no_errors',
          'format': email_format
      })
      sheet.conditional_format('B6:F6', {
          'type': 'no_errors',
          'format': split_format
      })
    
      b8_format = book.add_format({
          'left': 5,
          'border_color': '#57B223',
      })
      sheet.write("B8", "    REPORT TO:", b8_format)
    
      startrow, startcol = 8, 1
      for i, row in df_invoice_to.iterrows():
          sheet.write(startrow+i, startcol, f"    {row['col']}", b8_format)
    
      title_format = book.add_format({
          'align': 'right',
          'size': 24,
          'bold': True,
          'color': '#57B223'
      })
      sheet.write("F8", f"REPORT YEAR {self.year}", title_format)
      sheet.set_row(7, 24)
      date_format_start = book.add_format({
          "num_format": "dd/mm/yyyy",
          'align': 'right'
      })
      sheet.write('F9', pd.Timestamp(f"{self.year}-1"), date_format_start)
      sheet.write('F10', pd.date_range(f"{self.year}-12", periods=1, freq="M")[0], date_format_start)

    return {
      'status_code': response.status_code
    }