import requests
from mainapp.config import baseUrl
from mainapp.models.m_asset import Asset
import json
import datetime as dt

class Alarm(Asset):
  def __init__(self, token, assetId):
    super().__init__(token, assetId)
  
  def updateAlarmAttributes(self, alarmSetting):
    url = f'{baseUrl}/plugins/telemetry/ASSET/{self.assetId}/attributes/SERVER_SCOPE'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
      "Content-Type": "application/json"
    }
    alarmConditions = {}
    for param in alarmSetting:
      warning = "false"
      critical = "false"
        
      setting = alarmSetting[param]
      warningSetting = setting["warning"]
      criticalSetting = setting["critical"]
      if warningSetting["enable"] == True:
        warningRanges = warningSetting["ranges"]
        for range in warningRanges:
          upperValue = range["upper"]["value"]
          upperIs = range["upper"]["is"]
          lowerValue = range["lower"]["value"]
          lowerIs = range["lower"]["is"]
          upperCondition = "true" if upperIs == "No" else f"value {upperIs} {upperValue}"
          lowerCondition = "true" if lowerIs == "No" else f"value {lowerIs} {lowerValue}"
          condition = f"({lowerCondition} && {upperCondition})"
          warning = warning + " || " + condition

      if criticalSetting["enable"] == True:
       criticalRanges = criticalSetting["ranges"]
       for range in criticalRanges:
         upperValue = range["upper"]["value"]
         upperIs = range["upper"]["is"]
         lowerValue = range["lower"]["value"]
         lowerIs = range["lower"]["is"]
         upperCondition = "true" if upperIs == "No" else f"value {upperIs} {upperValue}"
         lowerCondition = "true" if lowerIs == "No" else f"value {lowerIs} {lowerValue}"
         condition = f"({lowerCondition} && {upperCondition})"
         critical = critical + " || " + condition
        
      alarmConditions[param] = {
       "warning": warning.replace("true && ", "").replace(" && true", "").replace("false || ", ""),
       "critical": critical.replace("true && ", "").replace(" && true", "").replace("false || ", "")
      }

    attributes = {
      "alarmSetting": alarmSetting,
      "alarmConditions": alarmConditions
    }
    data = json.dumps(attributes)
    response = requests.post(url=url, headers=headers, data=data)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': response.status_code
    }

