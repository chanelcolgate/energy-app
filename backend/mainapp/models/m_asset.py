import requests
from mainapp.config import baseUrl
import json
import datetime as dt

class Asset():
  def __init__(self, token, assetId):
    self.token = token
    self.assetId = assetId
    self.assetChildren = []
    self.formula = {} 

  def getFormulaElectric(self):
    url = f'{baseUrl}/plugins/telemetry/ASSET/{self.assetId}/values/attributes?keys=formulaForElectricityBill'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": 'application/json',
      "Content-Type": 'application/json'
    }
    response = requests.get(url=url, headers=headers)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    self.formula = response.json()[0]["value"]
    return {
      'status_code': response.status_code,
      'data': response.json()
    }

  def getAssetRelation(self):
    url = f'{baseUrl}/relations/info?fromId={self.assetId}&fromType=ASSET'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
      "Content-Type": "application/json"
    }
    response = requests.get(url=url, headers=headers)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    for elem in response.json():
      if elem['to']['entityType'] == 'ASSET':
        self.assetChildren.append({
          'name': elem['toName'],
          'id': elem['to']['id']
        })
    return {
      'status_code': response.status_code,
      'data': response.json()
    }
  
  def getAssetName(self):
    url = f'{baseUrl}/asset/info/{self.assetId}'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
    }
    response = requests.get(url=url, headers=headers)
    if response.status_code != requests.codes.ok:
      # raise Exception(f'getAssetName: {response.status_code}:{response.json()["message"]}')
      return {
        'status_code': response.status_code
      }
    # return response.json()["name"]
    return {
      'status_code': response.status_code,
      'data': response.json()['name']
    }
  
  def getAssetAdditionalInfor(self):
    url = f'{baseUrl}/plugins/telemetry/ASSET/{self.assetId}/values/attributes?key=additionalInfor'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
    }
    response = requests.get(url=url, headers=headers)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': response.status_code,
      'data': response.json()[0]["value"]
    }
  
  def updateAssetName(self, assetName):
    url = f'{baseUrl}/asset'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
      "Content-Type": "application/json"
    }
    data = json.dumps({
      "id": {
        "entityType": "ASSET",
        "id": self.assetId
      },
      "name": assetName,
      "type": "default"
    })
    response = requests.post(url=url, headers=headers, data=data)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': response.status_code
    }

  def updateAssetAdditionalInfor(self, addtionalInfor):
    url = f'{baseUrl}/plugins/telemetry/ASSET/{self.assetId}/attributes/SERVER_SCOPE'
    headers = {
      'X-Authorization': 'Bearer ' + self.token,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    data = json.dumps({
      "additionalInfor": addtionalInfor
    })
    response = requests.post(url=url, headers=headers, data=data)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': response.status_code
    }
  
  def deleteAsset(self):
    url = f'{baseUrl}/asset/{self.assetId}'
    headers = {
      'X-Authorization': 'Bearer ' + self.token,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    assetRelation = self.getAssetRelation()
    if assetRelation['status_code'] != 200:
      return {
        'status_code': assetRelation['status_code']
      }
    if len(assetRelation['data']) == 0:
      response = requests.delete(url=url, headers=headers)
      if response.status_code != requests.codes.ok:
        return {
          'status_code': response.status_code
        }
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': 406
    }
  
  def saveAssetInitTelemetry(self):
    url = f"{baseUrl}/plugins/telemetry/ASSET/{self.assetId}/timeseries/SERVER_SCOPE"
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
      "Content-Type": "application/json"
    }

    utcDatetime = dt.datetime.utcnow()
    offsetTime = dt.timedelta(hours=7)
    localDatetime = utcDatetime + offsetTime

    year = localDatetime.year
    month = localDatetime.month
    day = localDatetime.day
    hour = localDatetime.hour

    timestamp = int(localDatetime.timestamp()) * 1000
    thisDayTimestamp = int(dt.datetime(year=year, month=month, day=day).timestamp()) * 1000
    thisMonthTimestamp = int(dt.datetime(year=year, month=month, day=1).timestamp()) * 1000
    thisHourTimestamp = int(dt.datetime(year=year, month=month, day=day, hour=hour).timestamp()) * 1000

    data = json.dumps(
      [
        {"ts": timestamp, "values": {"energy": 0}},
        {
          "ts": thisMonthTimestamp,
          "values": {
            "firstEnergyValueOfMonth": 0,
            "lastEnergyValueOfMonth": 0,
            "firstPeakEnergyValueOfMonth": 0,
            "lastPeakEnergyValueOfMonth": 0,
            "firstNormalEnergyValueOfMonth": 0,
            "lastNormalEnergyValueOfMonth": 0,
            "firstOffPeakEnergyValueOfMonth": 0,
            "lastOffPeakEnergyValueOfMonth": 0
          },
        },
        {
          "ts": thisDayTimestamp,
          "values": {
            "firstEnergyValueOfDay": 0,
            "lastEnergyValueOfDay": 0,
            "firstPeakEnergyValueOfDay": 0,
            "lastPeakEnergyValueOfDay": 0,
            "firstNormalEnergyValueOfDay": 0,
            "lastNormalEnergyValueOfDay": 0,
            "firstOffPeakEnergyValueOfDay": 0,
            "lastOffPeakEnergyValueOfDay": 0
          },
        },
        {
          "ts": thisHourTimestamp,
          "values": {
            "firstEnergyValueOfHour": 0,
            "lastEnergyValueOfHour": 0
          },
        }
      ]
    )
    response = requests.post(url=url, headers=headers, data=data)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': response.status_code
    }

  def saveAssetAttributes(self, additionalInfor, formulaForElectricityBill):
    url = f"{baseUrl}/plugins/telemetry/ASSET/{self.assetId}/attributes/SERVER_SCOPE"
    headers = {
      'X-Authorization': 'Bearer ' + self.token,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    data = json.dumps({
      "additionalInfor": additionalInfor,  
      "formulaForElectricityBill": formulaForElectricityBill,
      "alarmSetting": {
        "activePower": {
          "warning": {
            "enable": False,  
            "ranges": []
          },
          "critical": {
            "enable": False,
            "ranges": []
          }
        },
        "current": {
          "warning": {
            "enable": False,
            "ranges": []
          },
          "critical": {
            "enable": False,
            "ranges": []
          }
        },
      },
      "alarmConditions": {
        "current": {
          "warning": "false",
          "critical": "false"
        },
        "activePower": {
          "warning": "false",
          "critical": "false"
        }
      }
    })
    response = requests.post(url=url, headers=headers, data=data)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': response.status_code
    }
  
  def saveRelation(self, parentInfor):
    url = f"{baseUrl}/relation"
    headers = {
      'X-Authorization': 'Bearer ' + self.token,
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
    data = json.dumps({
      "from": {
        "entityType": parentInfor["type"],
        "id": parentInfor["id"]
      },
      "to": {
        "entityType": "ASSET",
        "id": self.assetId
      },
      "type": "Contains"
    })
    response = requests.post(url=url, headers=headers, data=data)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': response.status_code
    }

class AssetNew():
  def __init__(self, token):
    self.token = token

  def createAsset(self, assetName):
    url = f'{baseUrl}/asset'
    headers = {
      'X-Authorization': 'Bearer ' + self.token,
      'Accept': "application/json",
      'Content-Type': "application/json"
    }
    data = json.dumps({
      "name": assetName,
      "type": "default"
    })
    response = requests.post(url=url, headers=headers, data=data)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code,
        'message': response.json()["message"]
      }
    return {
      'status_code': response.status_code,
      'data': response.json()['id']['id']
    }