from mainapp.config import baseUrl 
import requests
import json

class AuthLogin:
  def __init__(self, username, password):
    self.username = username
    self.password = password
  
  def getToken(self):
    url = f'{baseUrl}/auth/login'
    payload = json.dumps({
      "username": self.username,
      "password": self.password
    })
    headers= {
      'Content-Type': 'application/json'
    }

    response = requests.request("POST", url, headers=headers, data=payload)
    if response.status_code != requests.codes.ok:
      # raise Exception(f'login: {response.status_code}:{response.json()["message"]}')
      return {
        'status_code': response.status_code,
      }
    return  {
      'data': response.json(),
      'status_code': response.status_code
    }