from mainapp.config import baseUrl
from mainapp.models.m_asset import Asset
import requests
import json
import datetime as dt
import calendar
import pandas as pd

class Dashboard(Asset):
  def __init__(self, token, assetId, assetName):
    # self.token = token
    # self.assetId = assetId
    super().__init__(token, assetId)
    self.assetName = assetName
    self.consumption = []
    self.conumpstionPNO = []
  
  def readRecordsOfEveryIntervals(self, starTime, endTime, telemetryKeys):
    url = f'{baseUrl}/plugins/telemetry/ASSET/{self.assetId}/values/timeseries?limit=100&agg=NONE&orderBy=ASC&yseStrictDataTypes=true&keys={telemetryKeys}&startTs={starTime}&endTs={endTime}'
    headers = {
      "X-Authorization": 'Bearer ' + self.token,
      "Accept": "application/json",
      "Content-Type": "application/json"
    }
    response = requests.get(url=url, headers=headers)
    if response.status_code != requests.codes.ok:
      return {
        'status_code': response.status_code
      }
    return {
      'status_code': response.status_code,
      'data': response.json()
    }

  def readConsumption(self, startTime, endTime, interval):
    firstKey = ""
    lastKey = ""
    telemetryKeys = ""
    timeFormat = ""
    if interval == "1h":
      firstKey = "firstEnergyValueOfHour"
      lastKey = "lastEnergyValueOfHour"
      telemetryKeys = f"{firstKey},{lastKey}"
      timeFormat = "%H:00"
    if interval == "1d":
      firstKey = "firstEnergyValueOfDay"
      lastKey = "lastEnergyValueOfDay"
      telemetryKeys = f"{firstKey},{lastKey}"
      timeFormat ="%m-%d"
    if interval == "1M":
      firstKey = "firstEnergyValueOfMonth"
      lastKey = "lastEnergyValueOfMonth"
      telemetryKeys = f"{firstKey},{lastKey}"
      timeFormat = "%Y-%m"
    recordsOfEveryIntervals = self.readRecordsOfEveryIntervals(startTime, endTime, telemetryKeys)
    if recordsOfEveryIntervals['status_code'] != requests.codes.ok:
      return {
        'status_code': recordsOfEveryIntervals['status_code']
      }
    if len(recordsOfEveryIntervals['data']) == 0:
      return {
        'status_code': recordsOfEveryIntervals['status_code'],
        'data': pd.DataFrame(columns=["time", self.assetName])
      }
    else:
      # records = []
      # for i in range(len(recordsOfEveryInternals['data'][firstKey])):
      #   record = {}
      #   record["time"] = dt.datetime.fromtimestamp(recordsOfEveryInternals['data'][firstKey][i]['ts']/1000 + 3600*7).strftime(timeFormat)
      #   record[self.assetName] = int(recordsOfEveryInternals['data'][lastKey][i]['value']) - int(recordsOfEveryInternals['data'][firstKey][i]['value'])
      #   records.append(record)
      firstRecords = pd.DataFrame(recordsOfEveryIntervals['data'][firstKey]).rename(columns={"value": firstKey})
      lastRecords = pd.DataFrame(recordsOfEveryIntervals['data'][lastKey]).rename(columns={"value": lastKey})
      records = pd.merge(firstRecords, lastRecords, how="inner", on="ts")
      records["time"] = records["ts"].apply(lambda x: dt.datetime.fromtimestamp((x/1000 + 3600*7)).strftime(timeFormat))
      records[self.assetName] = pd.to_numeric(records[lastKey]) - pd.to_numeric(records[firstKey])
      return {
        'status_code': recordsOfEveryIntervals['status_code'],
        'data': records.drop([firstKey, lastKey, "ts"], axis=1)
      } 
      # return {
      #   'status_code': recordsOfEveryIntervals['status_code'],
      #   'data': pd.DataFrame(records)
      # }

  def getYearConsumption(self, year, quarter):
    utcDatetime = dt.datetime.utcnow().replace(tzinfo=dt.timezone.utc)
    offsetTime = dt.timedelta(hours=7)
    localDatetime = utcDatetime + offsetTime

    if int(quarter) == 0:
      startMonth = 1
      endMonth = 1
      if year == 'this year':
        year = localDatetime.year
      else:
        year = int(year)
      endYear = year + 1
    elif int(quarter) == 1:
      year = int(year)
      endYear = year
      startMonth = 1
      endMonth = 4
    elif int(quarter) == 2:
      year = int(year)
      endYear = year
      startMonth = 4
      endMonth = 7
    elif int(quarter) == 3:
      year = int(year)
      endYear = year
      startMonth = 7
      endMonth = 10
    elif int(quarter) == 4:
      year =int(year)
      endYear = year + 1
      startMonth = 10
      endMonth = 1

    # index = pd.date_range(start=f"{year}", end=f"{year + 1}", freq="M")
    # df_index = pd.DataFrame({"time": index}).set_index("time")
    # df_index.index = df_index.index.strftime("%Y-%m")
    # df_index.reset_index(inplace=True)

    # endTime = int(utcDatetime.timestamp())*1000
    # startTime = int(dt.datetime(year=year, month=1, day=1, tzinfo=dt.timezone.utc).timestamp() - 3600*7) * 1000 - 1
    index = pd.date_range(start=f"{year}-{startMonth}", end=f"{endYear}-{endMonth}", freq="M")
    df_index = pd.DataFrame({"time": index}).set_index("time")
    df_index.index = df_index.index.strftime("%Y-%m")
    df_index.reset_index(inplace=True)

    startTime = int(dt.datetime(year=year, month=startMonth, day=1, tzinfo=dt.timezone.utc).timestamp() -3600*7) * 1000 - 1
    endTime = int(dt.datetime(year=endYear, month=endMonth, day=1, tzinfo=dt.timezone.utc).timestamp() - 3600*7) * 1000 - 1
    interval = "1M"

    readConsumption = self.readConsumption(startTime, endTime, interval)
    if readConsumption['status_code'] != requests.codes.ok:
      return {
        'status_code': readConsumption['status_code']
      }
    df = readConsumption['data']
    super().getAssetRelation()
    for i in self.assetChildren:
      self.assetName = i['name']
      self.assetId = i['id']
      df_child = self.readConsumption(startTime, endTime, interval)
      df = pd.merge(df, df_child['data'], how='outer', on='time')
    df = df_index.merge(df, how='outer')
    self.consumption = json.loads(df.set_index('time').to_json(orient='table'))['data']
    return {
      'status_code': readConsumption['status_code'],
      'data': json.loads(df.set_index('time').to_json(orient='table'))['data']
    }
  
  def getMonthConsumption(self, year, month):
    utcDatetime = dt.datetime.utcnow().replace(tzinfo=dt.timezone.utc)
    offsetTime = dt.timedelta(hours=7)
    localDatetime = utcDatetime + offsetTime

    if year == "this year":
      year = localDatetime.year
      month = localDatetime.month
    else:
      year = int(year)
      month = int(month)

    endDayOfMonth = calendar.monthrange(year, month)[1]
    index = pd.date_range(start=f"{year}-{month}", end=f"{year}-{month}-{endDayOfMonth}", freq="D")
    df_index = pd.DataFrame({"time": index}).set_index("time")
    df_index.index = df_index.index.strftime("%m-%d")
    df_index.reset_index(inplace=True)

    endTime = int(pd.date_range(f"{year}-{month}-01", periods=1, freq="M", tz="Asia/Ho_Chi_Minh")[0].timestamp() * 1000)
    startTime = int(dt.datetime(year=year, month=month, day=1, tzinfo=dt.timezone.utc).timestamp() -3600*7)*1000 - 1
    interval = "1d"

    readConsumption = self.readConsumption(startTime, endTime, interval)
    if readConsumption['status_code'] != requests.codes.ok:
      return {
        'status_code': readConsumption['status_code']
      }
    df = readConsumption['data']
    super().getAssetRelation()
    for i in self.assetChildren:
      self.assetName = i['name']
      self.assetId = i['id']
      df_child = self.readConsumption(startTime, endTime, interval)
      df = pd.merge(df, df_child['data'], how='outer', on='time')
    df = df_index.merge(df, how='outer')
    self.consumption = json.loads(df.set_index('time').to_json(orient='table'))['data']
    return {
      'status_code': readConsumption['status_code'],
      'data': json.loads(df.set_index('time').to_json(orient='table'))['data']
    }
  
  def getDayConsumption(self):
    utcDatetime = dt.datetime.utcnow().replace(tzinfo=dt.timezone.utc)
    offsetTime = dt.timedelta(hours=7)
    localDatetime = utcDatetime + offsetTime

    year = localDatetime.year
    month = localDatetime.month
    day = localDatetime.day

    index = pd.date_range(start=f"{year}-{month}-{day} 00:00:00", end=f"{year}-{month}-{day} 23:59:59", freq="H")
    df_index = pd.DataFrame({"time": index}).set_index("time")
    df_index.index = df_index.index.strftime("%H:00")
    df_index.reset_index(inplace=True)

    endTime = int(utcDatetime.timestamp())*1000
    startTime = int(dt.datetime(year=year, month=month, day=day, tzinfo=dt.timezone.utc).timestamp() - 3600*7)*1000 - 1
    interval = "1h"
    
    readConsumption = self.readConsumption(startTime, endTime, interval)
    if readConsumption['status_code'] != requests.codes.ok:
      return {
        'status_code': readConsumption['status_code']
      }
    df = readConsumption['data']
    super().getAssetRelation()
    for i in self.assetChildren:
      self.assetName = i['name']
      self.assetId = i['id']
      df_child = self.readConsumption(startTime, endTime, interval)
      df = pd.merge(df, df_child['data'], how='outer', on='time')
    df = df_index.merge(df, how='outer')
    return {
      'status_code': readConsumption['status_code'],
      'data': json.loads(df.set_index('time').to_json(orient='table'))['data']
    }
  
  def readPNOConsumption(self, startTime, endTime, interval):
    firstPeakKey = ""
    firstNormalKey = ""
    firstOffPeakKey = ""
    lastNormalKey = ""
    lastOffPeakKey = ""
    telemetryKeys = ""
    timeFormat = ""
    if interval == "1d":
      firstPeakKey = "firstPeakEnergyValueOfDay"
      firstNormalKey = "firstNormalEnergyValueOfDay"
      firstOffPeakKey = "firstOffPeakEnergyValueOfDay"
      lastPeakKey = "lastPeakEnergyValueOfDay"
      lastNormalKey = "lastNormalEnergyValueOfDay"
      lastOffPeakKey = "lastOffPeakEnergyValueOfDay"
      telemetryKeys = f"{firstPeakKey},{firstNormalKey},{firstOffPeakKey},{lastPeakKey},{lastNormalKey},{lastOffPeakKey}"
      timeFormat = "%Y-%m-%d"
    if interval == "1M":
      firstPeakKey = "firstPeakEnergyValueOfMonth"
      firstNormalKey = "firstNormalEnergyValueOfMonth"
      firstOffPeakKey = "firstOffPeakEnergyValueOfMonth"
      lastPeakKey = "lastPeakEnergyValueOfMonth"
      lastNormalKey = "lastNormalEnergyValueOfMonth"
      lastOffPeakKey = "lastOffPeakEnergyValueOfMonth"
      telemetryKeys = f"{firstPeakKey},{firstNormalKey},{firstOffPeakKey},{lastPeakKey},{lastNormalKey},{lastOffPeakKey}"
      timeFormat = "%Y-%m"
    recordsOfEveryIntervals = self.readRecordsOfEveryIntervals(startTime, endTime, telemetryKeys)
    if recordsOfEveryIntervals['status_code'] != requests.codes.ok:
      return {'status_code': recordsOfEveryIntervals['status_code']}
    if len(recordsOfEveryIntervals['data']) == 0:
      return {
        'status_code': recordsOfEveryIntervals['status_code'],
        'data': pd.DataFrame(columns=["time", "peakConsumption", "normalConsumption", "offPeakConsumption"])
      }
    else:
      records = []
      for i in range(len(recordsOfEveryIntervals['data'][firstPeakKey])):
        record = {}
        record["time"] = dt.datetime.utcfromtimestamp(recordsOfEveryIntervals['data'][firstPeakKey][i]["ts"]/1000 + 3600*7).strftime(timeFormat)
        record["peakConsumption"] = int(recordsOfEveryIntervals['data'][lastPeakKey][i]["value"]) - int(recordsOfEveryIntervals['data'][firstPeakKey][i]["value"])
        record["normalConsumption"] = int(recordsOfEveryIntervals['data'][lastNormalKey][i]["value"]) - int(recordsOfEveryIntervals['data'][firstNormalKey][i]["value"])
        record["offPeakConsumption"] = int(recordsOfEveryIntervals['data'][lastOffPeakKey][i]["value"]) - int(recordsOfEveryIntervals['data'][firstOffPeakKey][i]["value"])
        records.append(record)
      return {
        'status_code': recordsOfEveryIntervals['status_code'],
        'data': pd.DataFrame(records)
      }

  def getMonthConsumptionPNO(self, year, month):
    utcDatetime = dt.datetime.utcnow().replace(tzinfo=dt.timezone.utc)
    offsetTime = dt.timedelta(hours=7)
    localDatetime = utcDatetime + offsetTime

    if year == 'this year':
      year = localDatetime.year
      month = localDatetime.month
    else:
      year = int(year)
      month = int(month)

    endDayOfMonth = calendar.monthrange(year, month)[1]
    index = pd.date_range(start=f"{year}-{month}", end=f"{year}-{month}-{endDayOfMonth}", freq="D")
    df_index = pd.DataFrame({"time": index}).set_index("time")
    df_index.index = df_index.index.strftime("%Y-%m-%d")
    df_index.reset_index(inplace=True)

    # endTime = int(utcDatetime.timestamp())*1000
    endTime = int(pd.date_range(f"{year}-{month}-01", periods=1, freq="M", tz="Asia/Ho_Chi_Minh")[0].timestamp() * 1000)
    startTime = int(dt.datetime(year=year, month=month, day=1, tzinfo=dt.timezone.utc).timestamp() - 3600*7)*1000 - 1
    interval = "1d"

    readPNOConsumption = self.readPNOConsumption(startTime, endTime, interval)
    if readPNOConsumption['status_code'] != requests.codes.ok:
      return {
        'status_code': readPNOConsumption['status_code']
      }
    df = readPNOConsumption['data']
    df = df_index.merge(df, how="outer")
    self.consumptionPNO = json.loads(df.set_index('time').to_json(orient='table'))['data']
    return {
      'status_code': readPNOConsumption['status_code'],
      'data': json.loads(df.set_index('time').to_json(orient='table'))['data']
    }
  
  def getYearConsumptionPNO(self, year, quarter):
    utcDatetime = dt.datetime.utcnow().replace(tzinfo=dt.timezone.utc)
    offsetTime = dt.timedelta(hours=7)
    localDatetime = utcDatetime + offsetTime

    if int(quarter) == 0:
      startMonth = 1
      endMonth = 1
      if year == 'this year':
        year = localDatetime.year
      else:
        year = int(year)
      endYear = year + 1
    elif int(quarter) == 1:
      year = int(year)
      endYear = year
      startMonth = 1
      endMonth = 4
    elif int(quarter) == 2:
      year = int(year)
      endYear = year
      startMonth = 4
      endMonth = 7
    elif int(quarter) == 3:
      year = int(year)
      endYear = year
      startMonth = 7
      endMonth = 10
    elif int(quarter) == 4:
      year = int(year)
      endYear = year + 1
      startMonth = 10
      endMonth = 1

    index = pd.date_range(start=f"{year}-{startMonth}", end=f"{endYear}-{endMonth}", freq="M")
    df_index = pd.DataFrame({"time": index}).set_index("time")
    df_index.index = df_index.index.strftime("%Y-%m")
    df_index.reset_index(inplace=True)

    startTime = int(dt.datetime(year=year, month=startMonth, day=1, tzinfo=dt.timezone.utc).timestamp() - 3600*7) * 1000 - 1
    endTime = int(dt.datetime(year=endYear, month=endMonth, day=1, tzinfo=dt.timezone.utc).timestamp() - 3600*7) * 1000 - 1
    interval = "1M"

    readPNOConsumption = self.readPNOConsumption(startTime, endTime, interval)
    if readPNOConsumption['status_code'] != requests.codes.ok:
      return {
        'status_code': readPNOConsumption['status_code'],
      }
    df = readPNOConsumption['data']
    df = df_index.merge(df, how="outer")
    self.consumptionPNO = json.loads(df.set_index('time').to_json(orient='table'))['data']
    return {
      'status_code': readPNOConsumption['status_code'],
      'data': json.loads(df.set_index('time').to_json(orient='table'))['data']
    }