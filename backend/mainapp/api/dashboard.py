from sanic import response
from sanic.exceptions import abort
from sanic import Blueprint
from mainapp.models.m_dashboard import Dashboard

dashboard = Blueprint('dashboard')

@dashboard.route('/api/dashboard/consumption/year', methods=['GET'])
async def dashboard_consumption_year(request):
  if request.method == 'GET':
    db = Dashboard(request.token, request.args.get('assetId'), request.args.get('assetName'))
    result = db.getYearConsumption(request.args.get("year"), request.args.get("quarter"))
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['data'])
  
@dashboard.route('/api/dashboard/consumption/month', methods=['GET'])
async def dashboard_consumption_month(request):
  if request.method == 'GET':
    db = Dashboard(request.token, request.args.get('assetId'), request.args.get('assetName'))
    result = db.getMonthConsumption(request.args.get("year"), request.args.get("month"))
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['data'])

@dashboard.route('/api/dashboard/consumption/today', methods=['GET'])
async def dashboard_consumption_today(request):
  if request.method == 'GET':
    db = Dashboard(request.token, request.args.get('assetId'), request.args.get('assetName'))
    result = db.getDayConsumption()
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['data'])

@dashboard.route('/api/dashboard/consumptionPNO/month', methods=['GET'])
async def dashboard_consumptionPNO_month(request):
  if request.method == 'GET':
    db = Dashboard(request.token, request.args.get('assetId'), request.args.get('assetName'))
    result = db.getMonthConsumptionPNO(request.args.get("year"), request.args.get("month"))
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['data'])

@dashboard.route('/api/dashboard/consumptionPNO/year', methods=['GET'])
async def dashboard_consumptionPNO_year(request):
  if request.method == 'GET':
    db = Dashboard(request.token, request.args.get('assetId'), request.args.get('assetName'))
    result = db.getYearConsumptionPNO(request.args.get("year"), request.args.get("quarter"))
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['data'])