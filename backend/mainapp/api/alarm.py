from sanic import response
from sanic.exceptions import abort
from sanic import Blueprint
from mainapp.models.m_alarm import Alarm

alarm = Blueprint('alarm')

@alarm.route('/api/asset/alarm/<assetId:string>', methods=['PUT'])
async def update_alarm(request, assetId):
  if request.method == 'PUT':
    alarm = Alarm(request.token, assetId)
    result = alarm.updateAlarmAttributes(request.json)
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['status_code'])