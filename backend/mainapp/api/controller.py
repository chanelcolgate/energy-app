from sanic import response
from sanic import Blueprint

controller = Blueprint('my_blueprint')

@controller.route('/my_bp')
def my_bp_func(request):
  print(request)
  return response.text('My First Blueprint')