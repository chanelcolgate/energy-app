from sanic import response
from sanic.exceptions import abort
from sanic import Blueprint
from mainapp.models.m_report import Report

report = Blueprint('report')

@report.route('/api/report', methods=['POST'])
async def report_create_report(request):
  if request.method == 'POST':
    r = Report(
      request.token,
      request.args.get('assetId'),
      request.args.get('assetName'),
      request.json.get('interval'),
      request.json.get('year'),
      request.json.get('month'),
      request.json.get('quarter')
    )
    if request.json.get('interval') == 'month':
      result = r.createReportMonth()
    elif request.json.get('interval') == 'quarter':
      result = r.createReportQuarter()
    elif request.json.get('interval') == 'year':
      result = r.createReportYear()
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['status_code'])