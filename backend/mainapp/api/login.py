from sanic import response
from sanic.exceptions import abort
from sanic import Blueprint
from mainapp.models.m_auth_login import AuthLogin

login = Blueprint('login')

@login.route('/api/login', methods=['GET'])
async def login_get_token(request):
  if request.method == 'GET':
    result = AuthLogin(request.args.get("username"), request.args.get("password")).getToken()
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['data'])