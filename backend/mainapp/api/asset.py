from sanic import response
from sanic.exceptions import abort
from sanic import Blueprint
from mainapp.models.m_asset import Asset, AssetNew 

asset = Blueprint('asset')

@asset.route('/api/asset/relation/<assetId:string>', methods=['GET'])
async def asset_get_relation(request, assetId):
  if request.method == 'GET':
    result = Asset(request.token, assetId).getAssetRelation()
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['data'])

@asset.route('/api/asset/generalInfor/<assetId:string>', methods=['GET', 'PUT', 'DELETE'])
async def asset_get_information(request, assetId):
  if request.method == 'GET':
    asset = Asset(request.token, assetId)
    resultName = asset.getAssetName()
    if resultName['status_code'] != 200:
      abort(resultName['status_code'])
    resultAdditionalInfor = asset.getAssetAdditionalInfor()
    if resultAdditionalInfor['status_code'] != 200:
      abort(resultAdditionalInfor['status_code'])
    return response.json({
      "name": resultName['data'],
      "additionalInfor": resultAdditionalInfor['data']
    })
  if request.method == 'PUT':
    asset = Asset(request.token, assetId)
    resultName = asset.updateAssetName(request.json.get('name'))
    if resultName['status_code'] != 200:
      abort(resultName['status_code'])
    resultAdditionalInfor = asset.updateAssetAdditionalInfor(request.json.get('additionalInfor'))
    if resultAdditionalInfor['status_code'] != 200:
      abort(resultAdditionalInfor['status_code'])
    return response.json(resultAdditionalInfor['status_code'])
  if request.method == 'DELETE':
    asset = Asset(request.token, assetId)
    result = asset.deleteAsset()
    if result['status_code'] != 200:
      abort(result['status_code'])
    return response.json(result['status_code'])
  
@asset.route('/api/asset', methods=['POST'])
async def asset_create(request):
  if request.method == 'POST':
    assetNew = AssetNew(request.token)
    resultCreateAsset = assetNew.createAsset(request.json.get('name'))
    if resultCreateAsset['status_code'] != 200:
      abort(resultCreateAsset['status_code'])
    asset = Asset(request.token, resultCreateAsset['data'])
    resultInitTelemetry = asset.saveAssetInitTelemetry()
    if resultInitTelemetry['status_code'] != 200:
      resultDeleteAsset = asset.deleteAsset()
      if resultDeleteAsset['status_code'] != 200:
        abort(resultDeleteAsset['status_code'])
      abort(resultInitTelemetry['status_code'])
    resultAttributes = asset.saveAssetAttributes(request.json.get('additionalInfor'), request.json.get('formulaForElectricityBill'))
    if resultAttributes['status_code'] != 200:
      resultDeleteAsset = asset.deleteAsset()
      if resultDeleteAsset['status_code'] != 200:
        abort(resultDeleteAsset['status_code'])
      abort(resultAttributes['status_code'])
    resultRelation = asset.saveRelation(request.json.get('parentInfor'))
    if resultRelation['status_code'] != 200:
      resultDeleteAsset = asset.deleteAsset()
      if resultDeleteAsset['status_code'] != 200:
        abort(resultDeleteAsset['status_code'])
      abort(resultRelation['status_code'])
    return response.json(resultRelation['status_code'])