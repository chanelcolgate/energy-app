from mainapp.api.controller import controller
from mainapp.api.asset import asset
from mainapp.api.login import login
from mainapp.api.dashboard import dashboard
from mainapp.api.report import report
from mainapp.api.alarm import alarm

all_blueprints = [controller, asset, login, dashboard, report, alarm]