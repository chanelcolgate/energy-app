import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

function load(component) {
  return () => import(`@/components/${component}`)
}

export default new Router({
  mode: 'history',
  routes: [
    {path: '/login', component: load('Login')},
    {
      path: '/', component: load('partial/Header'), children: [
        {path: '/', component: load('Dashboard/Dashboard')},
        {path: '/dashboard', component: load('Dashboard/Dashboard')},
        {path: '/analyst', component: load('Analyst/Analyst')},
        {path: '/alarm', component: load('Alarm/Alarm')},
        {path: '/report', component: load('Report/Report')},
        {path: '/entity_management', component: load('Management/Entity')},

        {path: '/user00101', component: load('Users/UserProfile')},

      ]
    },
  ]
})
