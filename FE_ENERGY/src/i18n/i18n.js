import Vue from 'vue'
import VueI18n from 'vue-i18n'

Vue.use(VueI18n)

const i18n = new VueI18n({
    locale: 'vi',
    messages: {
        'en': require('./en.json'),
        'vi': require('./vi.json')
    }
})

if (module.hot) {
    module.hot.accept(['./en.json', './vi.json'], () => {
        i18n.setLocaleMessage('en', require('./en.json'))
        i18n.setLocaleMessage('vi', require('./vi.json'))
    })
}

export default i18n