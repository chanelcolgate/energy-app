import '@/lib/css';
import '@/lib/script';
import '@/lib/custom.css';
// import '@/lib/sidebar.css';
import '@/lib/asset.css';

import Vue from 'vue';
import App from './App.vue';
import router from '@/router/';
import store from '@/store/';
import i18n from '@/i18n/i18n';
import VueSession from 'vue-session';
import axios from 'axios';
import VueAxios from 'vue-axios';
import moment from 'moment';
import VueMomentJS from 'vue-momentjs';
import vuetify from './plugins/vuetify';

import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';
Vue.use(Vuetify);

Vue.config.productionTip = false;

Vue.use(VueSession);
Vue.use(VueAxios, axios);
Vue.use(VueMomentJS, moment);

import global from '@/mixins/global'
Vue.mixin(global);

new Vue({
  router,
  store,
  i18n,
  vuetify,
  components: {
    App,
  },
  render: h => h(App)
}).$mount('#app');
