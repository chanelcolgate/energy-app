import axios from 'axios';
import global from '@/mixins/global';

const state = {
  pageCount: 0,
  items: [],
  alarmItems: [
    { title: 'Active', value: 0 },
    { title: 'critical', value: 0 },
    { title: 'warning', value: 0 },
  ],
  unAckItems: [
    { title: 'Not seen', value: 0 },
    { title: 'critical', value: 0 },
    { title: 'warning', value: 0 },
  ],
  costItems: [
    {
      id: 1,
      title: 'This year',
      status: false,
      rate: 15,
      cost: 12345
    },
    {
      id: 2,
      title: 'This month',
      status: false,
      rate: 15,
      cost: 12345
    },
    {
      id: 3,
      title: 'Today',
      status: true,
      rate: 1,
      cost: 12345
    },
  ],
  consumptionItems: [
    {
      id: 1,
      title: 'This year',
      status: false,
      rate: 15,
      cost: 12345
    },
    {
      id: 2,
      title: 'This month',
      status: false,
      rate: 15,
      cost: 1451
    },
    {
      id: 3,
      title: 'Today',
      status: true,
      rate: 1,
      cost: 34
    },
  ],
  labelBarChartYearConsumptions: [],
  datasetBarChartYearConsumptions: [],
  labelBarChartDayConsumptions: [],
  datasetBarChartDayConsumptions: [],
  labelPieChartYearConsumptions: [],
  datasetPieChartYearConsumptions: [],
  labelPieChartDayConsumptions: [],
  datasetPieChartDayConsumptions: [],
  labelBarChartMonthCost: [],
  datasetBarChartMonthCost: [],
  labelPieChartMonthCost: [],
  datasetPieChartMonthCost: [],
  dataLegendYearCharts: [],
  dataLegendDayCharts: [],
  levelMonthConsumption: [],
  datasetLoadCurve: [],
  labelLoadCurve: [],
  datasetCostByLevel: [],
  labelCostByLevel: [],
  maxCostByLevel: 0,
  temp1: 0,
  temp2: 0,
  temp3: 0,
  temp4: 0,
  temp5: 0,
  temp6: 0,
  backgroundColor: [
    '#ff6384', '#36a2eb', '#cc65fe',
    '#ffce56', '#1f77b4', '#ff7f0e',
    '#2ca02c', '#17becf', '#7f7f7f'
  ],
  backgroundColorForPeak: [
    '#ff6384', "#2ca02c", "#36a2eb"
  ],
  voltage: 220.23,
  current: 3.23,
  frequency: 50.12,
};

const mutations = {
  UPDATE_YEAR_CONSUMPTION(state, payload) {
    state.yearConsumption = payload;
  },
  UPDATE_MONTH_CONSUMPTION(state, payload) {
    state.monthConsumption = payload;
  },
  UPDATE_TODAY_CONSUMPTION(state, payload) {
    state.todayConsumption = payload;
  },
  UPDATE_LABEL_BAR_CHART_YEAR_CONSUMPTIONS(state, payload) {
    state.labelBarChartYearConsumptions = payload;
  },
  UPDATE_DATASET_BAR_CHART_YEAR_CONSUMPTIONS(state, payload) {
    state.datasetBarChartYearConsumptions = payload;
  },
  UPDATE_DATASET_BAR_CHART_DAY_CONSUMPTIONS(state, payload) {
    state.datasetBarChartDayConsumptions = payload;
  },
  UPDATE_LABEL_BAR_CHART_DAY_CONSUMPTIONS(state, payload) {
    state.labelBarChartDayConsumptions = payload;
  },
  UPDATE_LABEL_PIE_CHART_YEAR_CONSUMPTIONS(state, payload) {
    state.labelPieChartYearConsumptions = payload;
  },
  UPDATE_DATASET_PIE_CHART_YEAR_CONSUMPTIONS(state, payload) {
    state.datasetPieChartYearConsumptions = payload;
  },
  UPDATE_LABEL_PIE_CHART_DAY_CONSUMPTIONS(state, payload) {
    state.labelPieChartDayConsumptions = payload;
  },
  UPDATE_DATASET_PIE_CHART_DAY_CONSUMPTIONS(state, payload) {
    state.datasetPieChartDayConsumptions = payload;
  },
  UPDATE_DATA_LEGEND_YEAR_CHARTS(state, payload) {
    state.dataLegendYearCharts = payload;
  },
  UPDATE_DATA_LEGEND_DAY_CHARTS(state, payload) {
    state.dataLegendDayCharts = payload;
  },
  UPDATE_LEVEL_MONTH_CONSUMPTION(state, payload) {
    state.levelMonthConsumption = payload;
  },
  UPDATE_LABEL_BAR_CHART_MONTH_COST(state, payload) {
    state.labelBarChartMonthCost = payload;
  },
  UPDATE_DATASET_BAR_CHART_MONTH_COST(state, payload) {
    state.datasetBarChartMonthCost = payload;
  },
  UPDATE_LABEL_PIE_CHART_MONTH_COST(state, payload) {
    state.labelPieChartMonthCost = payload;
  },
  UPDATE_DATASET_PIE_CHART_MONTH_COST(state, payload) {
    state.datasetPieChartMonthCost = payload;
  },
  UPDATE_PAGE_COUNT(state, payload) {
    state.pageCount = payload;
  },
  UPDATE_ITEMS(state, payload) {
    state.items = payload;
  },
  UPDATE_DATASET_COST_BY_LEVEL(state, payload) {
    state.datasetCostByLevel = payload;
  },
  UPDATE_LABEL_COST_BY_LEVEL(state, payload) {
    state.labelCostByLevel = payload;
  },
  UPDATE_MAX_COST_BY_LEVEL(state, payload) {
    state.maxCostByLevel = payload;
  },
};

function getDataConsumptionPanel(obj, responseData, name, total) {
  obj.map((item) => {
    if (item.title === name) {
      responseData.forEach(elem => {
        total += elem.value;
      })
      item.cost = total;
    }
  })
}

function createDatasetsAndLabelsChartConsumption(obj, responseData) {
  let datasets = [], labels = [], data = [];
  // B1: Khoi tao datasets tu subAsset day vao
  obj.forEach(elem => {
    datasets.push({
      label: elem.name,
      data: [],
      barPercentage: 0.5,
    })
  })
  // B2: Tao gia tri other
  responseData.map(elem => {
    var other = elem.value * 3;
    for (let e in elem) {
      if (e !== 'time' && e !== 'value') {
        other = other - elem[e];
      }
    }
    elem.other = other;
    return elem;
  })
  // B3: Tao labels va data cua SubAsset va other
  responseData.forEach(elem => {
    labels.push(elem.time);
    datasets.forEach(e => {
      e.data.push(elem[e.label]);
    })
    data.push(elem.other);
  })
  // B4: Hoan thanh datasets
  datasets.push({
    label: 'Other',
    barPercentage: 0.5,
    data: data
  })
  // B5: them mau
  for (let i = 0; i < datasets.length; i++) {
    datasets[i].backgroundColor = state.backgroundColor[i];
  }
  return {
    datasets: datasets,
    labels: labels
  }
}

function createLevelMonthConsumption(payload) {
  var level = payload.map(i => {
    return {
    time: i.time,
    level1: (((i.value >= 0) && (i.value - 0)) - ((i.value >= 50000) && (i.value - 50000))),
    level2: (((i.value >= 50000) && (i.value - 50000)) - ((i.value >= 100000) && (i.value - 100000))),
    level3: (((i.value >= 100000) && (i.value - 100000)) - ((i.value >= 200000) && (i.value - 200000))),
    level4: (((i.value >= 200000) && (i.value - 200000)) - ((i.value >= 300000) && (i.value - 300000))),
    level5: (((i.value >= 300000) && (i.value - 300000)) - ((i.value >= 400000) && (i.value - 400000))),
    level6: (((i.value >= 400000) && (i.value - 400000)))
    };
  })
  return level;
}

const actions = {
  async getYearConsumption({state, commit}, assetId) {
    if(assetId === '') return;
    var config = {
      method: 'get',
      url: `${global.methods.baseUrlServer()}api/dashboard/consumption/year?assetId=${assetId}&assetName=value&year=this year&quarter=0`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };
    await axios(config)
      .then(function (response) {
        state.consumptionItems.map((item) => {
          if (item.title === 'This year') {
            var a = 0;
            for (let i in response.data) {
              a += response.data[i].value;
            }
            item.cost = a;
          }
        })
        // Create level month consumption
        var level = createLevelMonthConsumption(response.data);
        commit('UPDATE_LEVEL_MONTH_CONSUMPTION', level);
      })
      .catch(function (error) {
        console.log(error);
      });
  },
  async getMonthConsumption({state, commit}, {assetId, meters, name, prices, method}) {
    if(assetId === '') return;
    var config = {
      method: 'get',
      url: `${global.methods.baseUrlServer()}api/dashboard/consumption/month?assetId=${assetId}&assetName=${name}&month=this month&year=this year`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };
    await axios(config)
      .then(function (response) {
        state.consumptionItems.map((item) => {
          // Get data for consumption panel
          if (item.title === 'This month') {
            var datasets = [];
            var data = [];
            var labels = [];
            var datasetCostByLevel = [0];
            const reducer = (accumulator, currentValue) => accumulator + currentValue;
            var  dataLegendCharts = [];
            var count = 0;
            var costMonthByLevel = 0;
            meters.forEach(elem => {
              datasets.push({
                label: elem.name,
                data: [],
                barPercentage: 0.5,
              })
            });
            response.data.map(elem => {
              var other = elem[name] * 3;
              for (let e in elem) {
                if (e !== 'time' && e !== name) {
                  other = other - elem[e];
                }
              }
              elem.other = other;
              return elem;
            })
            response.data.forEach(elem => {
              count += elem[name];
              labels.push(elem.time);
              datasets.forEach(e => {
                e.data.push(elem[e.label]);
              })
              data.push(elem.other);
              if (method === 'byLevel') {
                if (elem[name] == null) {
                  datasetCostByLevel.push(null);
                  return
                }
                let costLevel1 = prices[0].price * ((((count >= 0) && (count - 0)) - ((count >= 50000) && (count - 50000))));
                let costLevel2 = prices[1].price * ((((count >= 50000) && (count - 50000)) - ((count >= 100000) && (count - 100000))));
                let costLevel3 = prices[2].price * ((((count >= 100000) && (count - 100000)) - ((count >= 200000) && (count - 200000))));
                let costLevel4 = prices[3].price * ((((count >= 200000) && (count - 200000)) - ((count >= 300000) && (count - 300000))));
                let costLevel5 = prices[4].price * ((((count >= 300000) && (count - 300000)) - ((count >= 400000) && (count - 400000))));
                let costLevel6 = prices[5].price * ((((count >= 400000) && (count - 400000))));
                costMonthByLevel = Math.floor((costLevel1 + costLevel2 + costLevel3 + costLevel4 + costLevel5 + costLevel6)/1000);
                datasetCostByLevel.push(costMonthByLevel);
              }
            })
            datasets.push({
              label: 'Other',
              barPercentage: 0.5,
              data: data,
            });
            for (let i = 0; i < datasets.length; i++) {
              datasets[i].backgroundColor = state.backgroundColor[i];
            }
            item.cost = count;
            commit('UPDATE_LABEL_BAR_CHART_YEAR_CONSUMPTIONS', labels);
            commit('UPDATE_DATASET_BAR_CHART_YEAR_CONSUMPTIONS', datasets);

            const timeNow = Date.now();
            const shiftedDateTime = new Date(timeNow + 7*3600*1000);
            const userDate = parseInt(shiftedDateTime.toJSON().substr(8, 2));
            state.costItems.map(item => {
              if (item.title === 'Today') {
                item.cost = Math.floor((datasetCostByLevel[userDate] - datasetCostByLevel[userDate-1]));
              }
            });

            labels.push('');
            commit('UPDATE_LABEL_COST_BY_LEVEL', labels);
            commit('UPDATE_DATASET_COST_BY_LEVEL', datasetCostByLevel);
            commit('UPDATE_MAX_COST_BY_LEVEL', costMonthByLevel);
            labels = [];
            count = 0;
            data = [];
            datasets.forEach(elem => {
              labels.push(elem.label);
              data.push(elem.data.reduce(reducer));
              elem.data.forEach(e => {
                if (e === null || e === 0) return;
                count += 1;
              })
              dataLegendCharts.push({
                text: elem.label,
                min: Math.min.apply(null, elem.data),
                max: Math.max.apply(null, elem.data),
                total: elem.data.reduce(reducer),
                average: Math.floor(elem.data.reduce(reducer)/count*100, 2)/100
              });
              count = 0;
            })
            data = data.map(elem => {
              return Math.floor(elem/data.reduce(reducer)*10000)/100;
            });
            datasets = [];
            datasets.push({
              data: data,
              backgroundColor: state.backgroundColor
            })
            commit('UPDATE_LABEL_PIE_CHART_YEAR_CONSUMPTIONS', labels);
            commit('UPDATE_DATASET_PIE_CHART_YEAR_CONSUMPTIONS', datasets);
            for (let i = 0; i < dataLegendCharts.length; i++ ) {
              dataLegendCharts[i].color = 'color: ' + state.backgroundColor[i];
            }
            commit('UPDATE_DATA_LEGEND_YEAR_CHARTS', dataLegendCharts);
          }
        });
      })
      .catch(function (error) {
        console.log(error);
      });
  },
  async getDayConsumption({state, commit}, {assetId, meters}) {
    if(assetId === '') return;
    var config = {
      method: 'get',
      url: `${global.methods.baseUrlServer()}api/dashboard/consumption/today?assetId=${assetId}&assetName=value`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      }
    };

    await axios(config)
    .then(function (response) {
      var count = 0;
      var datasets = [];
      var labels = [];
      var data = [];
      var dataLegendCharts = [];
      const reducer = (accumulator, currentValue) => accumulator + currentValue;
      getDataConsumptionPanel(state.consumptionItems, response.data, 'Today', count);
      var b = createDatasetsAndLabelsChartConsumption(meters, response.data);
      commit('UPDATE_DATASET_BAR_CHART_DAY_CONSUMPTIONS', b.datasets);
      commit('UPDATE_LABEL_BAR_CHART_DAY_CONSUMPTIONS', b.labels);
      b.datasets.forEach(elem => {
        labels.push(elem.label);
        data.push(elem.data.reduce(reducer));
        elem.data.forEach(e => {
          if (e === null || e === 0) return;
          count += 1;
        })
        dataLegendCharts.push({
          text: elem.label,
          min: Math.min.apply(null, elem.data),
          max: Math.max.apply(null, elem.data),
          total: elem.data.reduce(reducer),
          average: Math.floor(elem.data.reduce(reducer)/count*100, 2)/100
        });
        count = 0;
      })
      data = data.map(elem => {
        return Math.round(elem/data.reduce(reducer)*100, 2);
      })
      datasets.push({
        data: data,
        backgroundColor: state.backgroundColor
      })
      commit('UPDATE_LABEL_PIE_CHART_DAY_CONSUMPTIONS', labels);
      commit('UPDATE_DATASET_PIE_CHART_DAY_CONSUMPTIONS', datasets);
      for (let i = 0; i < dataLegendCharts.length; i++) {
        dataLegendCharts[i].color = 'color: ' + state.backgroundColor[i];
      }
      commit('UPDATE_DATA_LEGEND_DAY_CHARTS', dataLegendCharts);
    })
    .catch(function (error) {
      console.log(error);
    });
  },

  async getYearCostByTime({state}, {assetId, price}) {
    if(assetId === '') return;
    var config = {
      method: 'get',
      url: `${global.methods.baseUrlServer()}api/dashboard/consumptionPNO/year?assetId=${assetId}&assetName=value&year=this year&quarter=0`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token'),
        'Content-Type': 'application/json'
      },
    };
    await axios(config)
    .then(function (response) {
      var temp = 0;
      var total = 0;
      response.data.forEach(elem => {
        temp = elem.normalConsumption*price['Normal'] +
               elem.offPeakConsumption*price['Off-peak'] +
               elem.peakConsumption*price['Peak'];
        total += temp;
      });
      state.costItems.map(item => {
        if (item.title === 'This year') {
          item.cost = Math.floor(total/1000);
        }
      })
    })
    .catch(function (error) {
      console.log(error);
    });
  },

  async getYearCostByLevel({state}, {price, payload}) {
    var temp = 0;
    var yearCost = 0;
    var monthCost = 0;
    var timestamp = new Date().toLocaleDateString('en-US');
    payload.forEach(elem => {
      temp = elem.level1*price['Level 1'] +
      elem.level2*price['Level 2'] +
      elem.level3*price['Level 3'] +
      elem.level4*price['Level 4'] +
      elem.level5*price['Level 5'] +
      elem.level6*price['Level 6'];
      if (Number(elem.time.slice(-2)) == timestamp.match(/\d+/g)[0]) {
        state.temp1 = Math.floor(elem.level1*price['Level 1']/1000);
        state.temp2 = Math.floor(elem.level2*price['Level 2']/1000);
        state.temp3 = Math.floor(elem.level3*price['Level 3']/1000);
        state.temp4 = Math.floor(elem.level4*price['Level 4']/1000);
        state.temp5 = Math.floor(elem.level5*price['Level 5']/1000);
        state.temp6 = Math.floor(elem.level6*price['Level 6']/1000);
        monthCost = temp;
      }
      yearCost += temp;
    })
    state.costItems.map(item => {
      if (item.title === 'This year') {
        item.cost = Math.floor(yearCost/1000);
      }
      if (item.title === 'This month') {
        item.cost = Math.floor(monthCost/1000);
      }
    })
  },

  async getMonthCostByTime({commit, state}, {price, assetId}) {
    var config = {
      method: 'get',
      url: `${global.methods.baseUrlServer()}api/dashboard/consumptionPNO/month?assetId=${assetId}&assetName=value&year=this year&month=this month`,
      headers: {
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
      const reducer = (accumulator, currentValue) => accumulator + currentValue;
      var timestamp = new Date().toLocaleDateString('en-US');
      var barChartDatasets=[], barChartLabels=[], pieChartLabels=[], pieChartDatasets=[], data=[];
      var monthCost = 0;
      var todayCost = 0;
      for (let p in price) {
        pieChartLabels.push(p);
      }
      pieChartLabels.forEach(elem => {
        barChartDatasets.push({
          label: elem,
          data: [],
          barPercentage: 0.5
        });
      })
      response.data.forEach(elem => {
        barChartLabels.push(elem.time.match(/(\W)(\d+\W\d+)/)[2]);
        barChartDatasets.forEach(e => {
          if (e.label === 'Peak') {
            e.data.push(elem.peakConsumption);
          } else if (e.label === 'Off-peak') {
            e.data.push(elem.offPeakConsumption);
          } else if (e.label === 'Normal') {
            e.data.push(elem.normalConsumption);
          }
        });
      })

      for (let i=0; i<barChartDatasets.length; i++) {
        barChartDatasets[i].backgroundColor = state.backgroundColorForPeak[i];
      }

      commit('UPDATE_LABEL_BAR_CHART_MONTH_COST', barChartLabels);
      commit('UPDATE_DATASET_BAR_CHART_MONTH_COST', barChartDatasets);

      barChartDatasets.forEach(elem => {
        data.push(elem.data.reduce(reducer));
      })

      for (let i in data) {
        monthCost += data[i]*price[pieChartLabels[i]];
      }

      state.costItems.map(item => {
        if (item.title === 'This month') {
          item.cost = Math.floor(monthCost/1000);
        }
      })

      data = data.map(elem => {
        return Math.floor(elem/data.reduce(reducer)*10000)/100;
      })

      pieChartDatasets.push({
        backgroundColor: state.backgroundColorForPeak,
        data: data
      })

      commit('UPDATE_LABEL_PIE_CHART_MONTH_COST', pieChartLabels);
      commit('UPDATE_DATASET_PIE_CHART_MONTH_COST', pieChartDatasets);

      response.data.forEach(elem => {
        if (Number(elem.time.slice(-2)) == timestamp.match(/\d+/g)[1]) {
          todayCost = elem.normalConsumption*price["Normal"] +
                      elem.offPeakConsumption*price["Off-peak"] +
                      elem.peakConsumption*price["Peak"];
        }
      })

      state.costItems.map(item => {
        if (item.title === 'Today') {
          item.cost = Math.floor(todayCost/1000);
        }
      })

    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getLoadCurve({commit, state}, {assetId}) {
    if(assetId === '') return;
    var endTs = Date.now();
    var startTs = endTs - 3600*1000*24;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/plugins/telemetry/ASSET/${assetId}/values/timeseries?interval=300000&agg=AVG&orderBy=ASC&useStrictDataTypes=true&keys=activePower&startTs=${startTs}&endTs=${endTs}`,
      headers: {
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
      if (!Object.keys(response.data).length) return;
      var datasets = [], labels = [], data = [];
      response.data.activePower.forEach(elem => {
        var timestamp = '' + new Date(elem.ts);
        timestamp = timestamp.match(/\d+:\d+/g)[0];
        data.push(elem.value);
        labels.push(timestamp);
      });
      datasets.push({
        data: data,
        borderColor: '#6B7FD0',
        backgroundColor: '#6B8FD0',
        pointRadius: 0,
      });
      state.datasetLoadCurve = datasets;
      state.labelLoadCurve = labels;
    })
    .catch(function (error) {
      console.log(error);
    });
  },

  async getVCF({commit, state}, {assetId}) {
    if(assetId === '') return;
    var timestamp = Date.now() - 3600*1000*24;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/plugins/telemetry/ASSET/${assetId}/values/timeseries?useStrictDataTypes=true&keys=voltage,current,frequency`,
      headers: {
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
      if (response.data.voltage[0].ts > timestamp) {
        state.voltage = response.data.voltage[0].value.toFixed(2);
      }
      if (response.data.current[0].ts > timestamp) {
        state.current = response.data.current[0].value.toFixed(2);
      }
      if (response.data.frequency[0].ts > timestamp) {
        state.frequency = response.data.frequency[0].value.toFixed(2);
      }
    })
    .catch(function (error) {
      console.log(error);
    });
  },

  async getAlarmActiveItem({commit, state}, {assetId}) {
    var endTs = Date.now();
    var startTs = endTs - 3600*1000*24;
    if(assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/alarm/ASSET/${assetId}?searchStatus=ACTIVE&pageSize=1&page=0&textSearch&startTime=${startTs}&endTime=${endTs}&fetchOriginator=true`,
      headers: { 
        'X-Authorization': 'Bearer ' + localStorage.getItem('token') 
      }
    };

    await axios(config)
    .then(function (response) {
      state.alarmItems.forEach(elem => {
        if (elem.title === 'Active') {
          elem.value = response.data.totalElements;
        }
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getAlarmCriticalItem({commit, state}, {assetId}) {
    var endTs = Date.now();
    var startTs = endTs - 3600*1000*24;
    if(assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/alarm/ASSET/${assetId}?searchStatus=ACTIVE&pageSize=1&page=0&textSearch=CRITICAL&startTime=${startTs}&endTime=${endTs}&fetchOriginator=true`,
      headers: { 
        'X-Authorization': 'Bearer ' + localStorage.getItem('token') 
      }
    };

    await axios(config)
    .then(function (response) {
      state.alarmItems.forEach(elem => {
        if (elem.title === 'critical') {
          elem.value = response.data.totalElements;
        }
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getAlarmWarningItem({commit, state}, {assetId}) {
    var endTs = Date.now();
    var startTs = endTs - 3600*1000*24;
    if(assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/alarm/ASSET/${assetId}?searchStatus=ACTIVE&pageSize=1&page=0&textSearch=WARNING&startTime=${startTs}&endTime=${endTs}&fetchOriginator=true`,
      headers: { 
        'X-Authorization': 'Bearer ' + localStorage.getItem('token') 
      }
    };

    await axios(config)
    .then(function (response) {
      state.alarmItems.forEach(elem => {
        if (elem.title === 'warning') {
          elem.value = response.data.totalElements;
        }
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getUnackItem({state}, {assetId}) {
    var endTs = Date.now();
    var startTs = endTs - 3600*1000*24;
    if (assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/alarm/ASSET/${assetId}?searchStatus=UNACK&pageSize=1&page=0&textSearch&startTime=${startTs}&endTime=${endTs}&fetchOriginator=true`,
      headers: { 
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
      state.unAckItems.forEach(elem => {
        if (elem.title === 'Not seen') {
          elem.value = response.data.totalElements;
        }
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getUnackCriticalItem({state}, {assetId}) {
    var endTs = Date.now();
    var startTs = endTs - 3600*1000*24;
    if (assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/alarm/ASSET/${assetId}?searchStatus=UNACK&pageSize=1&page=0&textSearch=CRITICAL&startTime=${startTs}&endTime=${endTs}&fetchOriginator=true`,
      headers: { 
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
      state.unAckItems.forEach(elem => {
        if (elem.title === 'critical') {
          elem.value = response.data.totalElements;
        }
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getUnackWarningItem({state}, {assetId}) {
    var endTs = Date.now();
    var startTs = endTs - 3600*1000*24;
    if (assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/alarm/ASSET/${assetId}?searchStatus=UNACK&pageSize=1&page=0&textSearch=WARNING&startTime=${startTs}&endTime=${endTs}&fetchOriginator=true`,
      headers: { 
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
      state.unAckItems.forEach(elem => {
        if (elem.title === 'warning') {
          elem.value = response.data.totalElements;
        }
      });
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getAlarmTable({state, commit}, {assetId, page=0}) {
    if (assetId === '') return;
    var userOffsetTime = 7*3600*1000;
    //var endTs = Date.now();
    //var shiftedDateTime = new Date(endTs + userOffsetTime);
    //var startTs = Date.parse(shiftedDateTime.toJSON().substr(0, 10)) - userOffsetTime;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/alarm/ASSET/${assetId}?pageSize=5&page=${page}&startTime&endTime&fetchOriginator=true`,
      headers: { 
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
      commit('UPDATE_PAGE_COUNT', response.data.totalPages);
      var items = [];
      function convertTimestamp(ts) {
        var date = new Date(ts);
        return date.getDate() +
               "/" + (date.getMonth()+1) +
               "/" + date.getFullYear() +
               " " + date.getHours() +
               ":" + date.getMinutes() +
               ":" + date.getSeconds();
      }
      response.data.data.forEach(elem => {
        var string = elem.name.match(/(.*)\|(.*)/);
        items.push({
          createdTime: convertTimestamp(elem.createdTime),
          originatorName: elem.originatorName,
          type: string[1],
          severity: string[2],
          id: elem.id.id,
          status: elem.status,
          details: elem.details,
          startTs: convertTimestamp(elem.startTs),
          endTs: convertTimestamp(elem.endTs)
        });
      });
      commit('UPDATE_ITEMS', items);
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async postAlarmClear({state}, {id}) {
    var config = {
      method: 'post',
      url: `http://103.28.32.80:8080/api/alarm/${id}/clear`,
      headers: {
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async postAlarmAck({state}, {id}) {
    var config = {
      method: 'post',
      url: `http://103.28.32.80:8080/api/alarm/${id}/ack`,
      headers: {
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
    })
    .catch(function (error) {
      console.log(error);
    });
  },
};

const getters = {
  consumptionItems: state => state.consumptionItems,
  costItems: state => state.costItems,
  labelBarChartYearConsumptions: state => state.labelBarChartYearConsumptions,
  datasetBarChartYearConsumptions: state => state.datasetBarChartYearConsumptions,
  labelBarChartDayConsumptions: state => state.labelBarChartDayConsumptions,
  datasetBarChartDayConsumptions: state => state.datasetBarChartDayConsumptions,
  labelPieChartYearConsumptions: state => state.labelPieChartYearConsumptions,
  datasetPieChartYearConsumptions: state => state.datasetPieChartYearConsumptions,
  labelPieChartDayConsumptions: state => state.labelPieChartDayConsumptions,
  datasetPieChartDayConsumptions: state => state.datasetPieChartDayConsumptions,
  labelBarChartMonthCost: state => state.labelBarChartMonthCost,
  datasetBarChartMonthCost: state => state.datasetBarChartMonthCost,
  labelPieChartMonthCost: state => state.labelPieChartMonthCost,
  datasetPieChartMonthCost: state => state.datasetPieChartMonthCost,
  dataLegendYearCharts: state => state.dataLegendYearCharts,
  dataLegendDayCharts: state => state.dataLegendDayCharts,
  levelMonthConsumption: state => state.levelMonthConsumption,
  datasetLoadCurve: state => state.datasetLoadCurve,
  labelLoadCurve: state => state.labelLoadCurve,
  voltage: state => state.voltage,
  current: state => state.current,
  frequency: state => state.frequency,
  alarmItems: state => state.alarmItems,
  unAckItems: state => state.unAckItems,
  pageCount: state => state.pageCount,
  items: state => state.items,
  datasetCostByLevel: state => state.datasetCostByLevel,
  labelCostByLevel: state => state.labelCostByLevel,
  maxCostByLevel: state => state.maxCostByLevel,
  temp1: state => state.temp1,
  temp2: state => state.temp2,
  temp3: state => state.temp3,
  temp4: state => state.temp4,
  temp5: state => state.temp5,
  temp6: state => state.temp6,
};

const dashboardModule = {
  state,
  mutations,
  actions,
  getters
};

export default dashboardModule;
