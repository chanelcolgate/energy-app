import axios from 'axios';
import global from '@/mixins/global';
import JSZip from 'jszip';
window.JSZip = JSZip;

const state = {
  result: 200,
};

const mutations = {
  UPDATE_RESULT(state, payload) {
    state.result = payload;
  },
};

const actions = {
  async createReport({state, commit}, {assetId, assetName, interval, month, year, quarter}) {
    if(state.assetId === '') return;
    var data = JSON.stringify({
      "interval": interval,
      "month": month,
      "year": year,
      "quarter": quarter
    });

    var config = {
      method: 'post',
      url: `${global.methods.baseUrlServer()}api/report?assetId=${assetId}&assetName=${assetName}`,
      headers: { 
        'Authorization': 'Bearer ' + localStorage.getItem('token'), 
        'Content-Type': 'application/json'
      },
      data : data
    };
    
    axios(config)
    .then(function (response) {
      console.log(response.data);
      commit('UPDATE_RESULT', response.data);
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getReport() {
    var config = {
      method: 'get',
      url: `${global.methods.baseUrlServer()}file_report/report_template.xlsx`,
      responseType: 'blob',
    };
    window.open(config.url);
    await axios(config)
    .then(function(response) {
      // var fileURL = window.URL.createObjectURL(new Blob([response.data]));
      // var fileLink = document.createElement('a');

      // fileLink.href = fileURL;
      // fileLink.setAttribute('download', 'file.xlsx')
      // document.body.appendChild(fileLink);

      // fileLink.click();
    })
    .catch(function (error) {
      console.log(error);
    });
  },
};

const getters = {
  result: state => state.result,
};

const reportModule = {
  state,
  mutations,
  actions,
  getters
}

export default reportModule;