import global from '@/mixins/global'

const state = {
  userList: [],
  userListAll: [],
  userSelected: {},


};

const actions = {

};
const mutations = {
  resetAllSelected(state){
    state.userSelected = {};
  },
};
const getters = {};
export default {
  state,
  getters,
  actions,
  mutations
}
