const state = { // data
  is_add: false,
  loading: false,
  headertitle: '',
  searchTxt: '',
  dialog_information: false,
  dialog_formular: false,
  dialog_alarmsetting: false,
  dialog_kpi: false,
  dialog_addmeter: false,
  dialog_addasset: false,
  dialog_meterinformation: false,
  alarm_when_enable_change: true,
  dialog_alarm_detail: false,
  group_buttons_ack_clear: false,
};
const actions = { // methods

};
const mutations = {
  setLoading(state, value) {
    state.loading = value;
  },
  setHeader(state, value) {
    state.headertitle = value;
  },
  resetSearch(state){
    state.searchTxt = '';
  },
};

export default {
  state,
  actions,
  mutations
};
