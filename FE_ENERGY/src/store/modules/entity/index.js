import axios from 'axios';
import global from '@/mixins/global'

const state = {
  name: '',
  additionalInfor: [],
  method: 'byLevel',
  method_default: 'byLevel',
  prices: [],
  assets: [],
  meters: [],
  token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaW5kZW5lcmd5cHJvamVjdEBleGFtcGxlLmNvbSIsInNjb3BlcyI6WyJURU5BTlRfQURNSU4iXSwidXNlcklkIjoiMDU4NWYzYjAtN2JmOS0xMWViLWI4ZGQtZWI1NWUyMWEwYjEzIiwiZmlyc3ROYW1lIjoiVXNlciBBIiwiZW5hYmxlZCI6dHJ1ZSwiaXNQdWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6ImRiZGJlN2UwLTdiZjgtMTFlYi1iOGRkLWViNTVlMjFhMGIxMyIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTYxOTA2NjA5MywiZXhwIjoxNjE5MDc1MDkzfQ.3eP4q6AVrZGXB9I3J0g80FkLKT5E_4dGIG2qVXX-b1ylAHdwAWPdKbvAKmqorf3ocJpPU4xQ7XwSGslFpvY9nw',
  // assetId: 'e51f4000-a807-11eb-9f25-398f4b7e9777',
  is_add_root: false,
  assetId: '',
  currentWarningRanges: [],
  currentCriticalRanges: [],
  currentWarningSwitchAlarm: false,
  currentCriticalSwitchAlarm: false,
  powerWarningRanges: [],
  powerCriticalRanges: [],
  powerWarningSwitchAlarm: false,
  powerCriticalSwitchAlarm: false,
  meterInformation: {},
  rssi_data:{"labels": [], "datasets": [{"data": [],"borderColor": "#6B7FD0", "backgroundColor": "#6B8FD0", "pointRadius": 0}]},
  sensorStatus: [],
  tree: [],
};

const mutations = {
  UPDATE_ASSETS(state, payload) {
    state.assets = payload;
  },
  UPDATE_METERS(state, payload) {
    state.meters = payload;
  },
  UPDATE_METHOD(state, payload) {
    state.method = payload;
    state.method_default = payload;
  },
  UPDATE_PRICES(state, payload) {
    state.prices = payload;
  },
  UPDATE_ASSETID(state, payload) {
    state.assetId = payload;
    localStorage.setItem("assetId", payload)
  },
  UPDATE_NAME(state, payload) {
    state.name = payload;
    localStorage.setItem("assetName", payload)
  },
  UPDATE_ADDITIONALINFOR(state, payload) {
    state.additionalInfor = payload;
  },
  UPDATE_TREE(state, payload) {
    state.tree = payload;
  },
};

const actions = {
  async getGeneralInformation({ state, commit }) {
    if(state.assetId === '') return;
    // let data = {assetId: state.assetId, token: localStorage.getItem('token')};
    // let link = global.methods.baseurl() + 'asset/general_info/';
    // let response = await global.methods.request(link, data, 'get');
    // state.name = response['name'];
    // state.additionalInfor = response['additionalInfor'];
    var config = {
      method: 'get',
      url: `${global.methods.baseUrlServer()}api/asset/generalInfor/${state.assetId}`,
      headers: { 
        'Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };

    await axios(config)
    .then(function (response) {
      commit('UPDATE_NAME', response.data['name']);
      commit('UPDATE_ADDITIONALINFOR', response.data['additionalInfor'])
    })
    .catch(function (error) {
      console.log(error);
    });
  },
  async getTree({commit}){
    var axios = require('axios');
    var config = {
      method: 'get',
      url: 'http://103.28.32.80:5000/api/tree',
      headers: {
        'token': localStorage.getItem('token')
      }
    };

    axios(config)
      .then(function (response) {
        commit('UPDATE_TREE', response.data);
        if(localStorage.getItem('assetId') == null){
          commit("UPDATE_ASSETID", response.data[0]['id']);
          commit("UPDATE_NAME", response.data[0]['name']);
        }else{
          commit("UPDATE_ASSETID", localStorage.getItem("assetId"));
          commit("UPDATE_NAME", localStorage.getItem("assetName"));
        }

      })
      .catch(function (error) {
        console.log(error);
      });
  },
  async getFormulaElectric({ state, commit }) {
    if(state.assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/plugins/telemetry/ASSET/${state.assetId}/values/attributes?keys=formulaForElectricityBill`,
      headers: {
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      },
    };
    commit('UPDATE_METHOD', '');
    commit('UPDATE_PRICES', []);
    await axios(config)
      .then((response) => {
        commit('UPDATE_METHOD', response.data[0].value.method);
        commit('UPDATE_PRICES', response.data[0].value.prices);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  async getComponents({ commit }) {
    if(state.assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/relations/info?fromId=${state.assetId}&fromType=ASSET`,
      headers: {
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };
    await axios(config)
      .then((response) => {
        var meters = [];
        var assets = [];
        response.data.forEach(elem => {
          if (elem.to.entityType === "DEVICE") {
            meters.push({
              name: elem.toName,
              id: elem.to.id
            });
          }
          if (elem.to.entityType === "ASSET") {
            assets.push({
              name: elem.toName,
              id: elem.to.id
            });
          }
        });
        // state.meters = meters;
        // state.assets = assets;
        commit('UPDATE_ASSETS', assets);
        commit('UPDATE_METERS', meters);
      })
      .catch((error) => {
        console.log(error);
      });
  },
  getAlarmSetting({ state }) {
    if(state.assetId === '') return;
    var config = {
      method: 'get',
      url: `http://103.28.32.80:8080/api/plugins/telemetry/ASSET/${state.assetId}/values/attributes?keys=alarmSetting`,
      headers: {
        'X-Authorization': 'Bearer ' + localStorage.getItem('token')
      }
    };
    axios(config)
      .then((response) => {
        state.currentWarningRanges = response.data[0].value.current.warning.ranges;
        state.currentCriticalRanges = response.data[0].value.current.critical.ranges;
        state.currentWarningSwitchAlarm = response.data[0].value.current.warning.enable;
        state.currentCriticalSwitchAlarm = response.data[0].value.current.critical.enable;
        state.powerWarningRanges = response.data[0].value.activePower.warning.ranges;
        state.powerCriticalRanges = response.data[0].value.activePower.critical.ranges;
        state.powerWarningSwitchAlarm = response.data[0].value.activePower.warning.enable;
        state.powerCriticalSwitchAlarm = response.data[0].value.activePower.critical.enable;
      })
      .catch((error) => {
        console.log(error);
      });
  },
};

const getters = {
  name: state => state.name,
  additionalInfor: state => state.additionalInfor,
  method: state => state.method,
  prices: state => state.prices,
  meters: state => state.meters,
  assets: state => state.assets,
  currentWarningRanges: state => state.currentWarningRanges,
  currentCriticalRanges: state => state.currentCriticalRanges,
  currentWarningSwitchAlarm: state => state.currentWarningSwitchAlarm,
  currentCriticalSwitchAlarm: state => state.currentCriticalSwitchAlarm,
  powerWarningRanges: state => state.powerWarningRanges,
  powerCriticalRanges: state => state.powerCriticalRanges,
  powerWarningSwitchAlarm: state => state.powerWarningSwitchAlarm,
  powerCriticalSwitchAlarm: state => state.powerCriticalSwitchAlarm,
  assetId: state => state.assetId,
};

const entityModule = {
  state,
  mutations,
  actions,
  getters
}

export default entityModule;
