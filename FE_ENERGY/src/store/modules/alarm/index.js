import axios from 'axios';
import global from '@/mixins/global';

const state = {

};

const mutations = {

};

const actions = {

};

const getters = {

};

const reportModule = {
  state,
  mutations,
  actions,
  getters
}

export default reportModule;
