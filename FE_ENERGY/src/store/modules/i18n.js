const state = { // data
  lang: 'vi'
};
const actions = { // methods
  changeLanguage(state, langParam) {
    state.lang = langParam;
  }
};
const mutations = {};
const getters = {}; // Mình sẽ không xài getters
export default {
  state,
  getters,
  actions,
  mutations
};
