import Vue from 'vue';
import Vuex from 'vuex';
import controllers from '@/store/modules/controllers';
import models from '@/store/modules/models';
import i18n from '@/store/modules/i18n';
import entity from '@/store/modules/entity';
import dashboard from '@/store/modules/dashboard';
import report from '@/store/modules/report';
import alarm from '@/store/modules/alarm';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    controllers,
    models,
    i18n,
    entity,
    dashboard,
    report,
    alarm,
  },
  // state: {
  //   name: '',
  //   additionalInfor: [],
  //   method: '',
  //   prices: [],
  //   currentWarningRanges: [],
  //   currentCriticalRanges: [],
  //   powerWarningRanges: [],
  //   powerCriticalRanges: [],
  //   assets: [],
  //   meters: [],
  //   token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaW5kZW5lcmd5cHJvamVjdEBleGFtcGxlLmNvbSIsInNjb3BlcyI6WyJURU5BTlRfQURNSU4iXSwidXNlcklkIjoiMDU4NWYzYjAtN2JmOS0xMWViLWI4ZGQtZWI1NWUyMWEwYjEzIiwiZmlyc3ROYW1lIjoiVXNlciBBIiwiZW5hYmxlZCI6dHJ1ZSwiaXNQdWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6ImRiZGJlN2UwLTdiZjgtMTFlYi1iOGRkLWViNTVlMjFhMGIxMyIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTYxODg5Mzc1NiwiZXhwIjoxNjE4OTAyNzU2fQ.Zm_kqXOpStnSJTixBteyLbutaSJQXoE2wkAsh3emsMtyH9fBF-yh1N-r1akKhEMgFulavKHdpHQ_ZpYskd8iOg',
  //   assetId: '7b9f32a0-a192-11eb-b183-93740c0d79ca',
  // },
  // getters: {},
  // actions: {},
  // mutations: {}
});

