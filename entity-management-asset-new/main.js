const state = {
    name: '',
    additionalInfor: [],
    method: '',
    prices: [],
    currentWarningRanges: [],
    currentCriticalRanges: [],
    powerWarningRanges: [],
    powerCriticalRanges: [],
    assets: [],
    meters: [],
};

const mutations = {
    UPDATE_NAME(state, payload) {
        state.name = payload;
    },
    UPDATE_ADDITIONALINFOR(state, payload) {
        state.additionalInfor = payload;
    },
    UPDATE_METHOD(state, payload) {
        state.method = payload;
    },
    UPDATE_PRICES(state, payload) {
        state.prices = payload;
    },
    UPDATE_CURRENT_WARNING_RANGES(state, payload) {
        state.currentWarningRanges = payload;
    },
    UPDATE_CURRENT_CRITICAL_RANGES(state, payload) {
        state.currentCriticalRanges = payload;
    },
    UPDATE_POWER_WARNING_RANGES(state, payload) {
        state.powerWarningRanges = payload;
    },
    UPDATE_POWER_CRITICAL_RANGES(state, payload) {
        state.powerCriticalRanges = payload;
    },
    UPDATE_ASSETS(state, payload) {
        state.assets = payload;
    },
    UPDATE_METERS(state, payload) {
        state.meters = payload;
    },
};

const actions = {
    getGeneralInformation({ commit }) {
        var config = {
            method: 'get',
            url: 'http://192.168.1.17:5000/api/asset/generalInfor/fe881370-9621-11eb-b183-93740c0d79ca',
            headers: {
                'token': 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJURVNUdGVuYW50QGlvdG1pbmQudm4iLCJzY29wZXMiOlsiVEVOQU5UX0FETUlOIl0sInVzZXJJZCI6IjIzNGEzOTcwLTQ5OGYtMTFlYi05NmNhLTg5YzcwYThmZTgyMSIsImVuYWJsZWQiOnRydWUsImlzUHVibGljIjpmYWxzZSwidGVuYW50SWQiOiJlNmM2YmQyMC00OThlLTExZWItOTZjYS04OWM3MGE4ZmU4MjEiLCJjdXN0b21lcklkIjoiMTM4MTQwMDAtMWRkMi0xMWIyLTgwODAtODA4MDgwODA4MDgwIiwiaXNzIjoidGhpbmdzYm9hcmQuaW8iLCJpYXQiOjE2MTgwNDM1MTUsImV4cCI6MTYxODA1MjUxNX0.wYSH6kVfGw_1G6FTHVtaECxn8zjKLcmBC6XZh5Ua_lRAKrZiapVBqBwOGJ9syEtTMvx7UxpcETI5Dg8I-JPX5g'
            }
        }
        axios(config).then((response) => {
            commit('UPDATE_NAME', response.data.name);
            commit('UPDATE_ADDITIONALINFOR', response.data.additionalInfor);
        })
        .catch((error) => {
            console.log(error);
        });
    },
    getFormulaElectric({ commit }) {
        var config = {
            method: 'get',
            url: 'http://103.28.32.80:8080/api/plugins/telemetry/ASSET/fe881370-9621-11eb-b183-93740c0d79ca/values/attributes?keys=formulaForElectricityBill',
            headers: { 
                'X-Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaW5kZW5lcmd5cHJvamVjdEBleGFtcGxlLmNvbSIsInNjb3BlcyI6WyJURU5BTlRfQURNSU4iXSwidXNlcklkIjoiMDU4NWYzYjAtN2JmOS0xMWViLWI4ZGQtZWI1NWUyMWEwYjEzIiwiZmlyc3ROYW1lIjoiVXNlciBBIiwiZW5hYmxlZCI6dHJ1ZSwiaXNQdWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6ImRiZGJlN2UwLTdiZjgtMTFlYi1iOGRkLWViNTVlMjFhMGIxMyIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTYxODA0Mzg1NywiZXhwIjoxNjE4MDUyODU3fQ.y1gN5WJArefBKPn6ywqQFtKO0tD38K0UixQFFXpOLTq3mYJW9cZHYKEYay8_MrxllHbVdVvoqWVYbJs_x-zKIg'
            }
        };
        axios(config).then((response) => {
            commit('UPDATE_METHOD', response.data[0].value.method);
            commit('UPDATE_PRICES', response.data[0].value.prices);
        })
        .catch((error) => {
            console.log(error);
        });
    },
    getAlarmSetting({ commit }) {
        var config = {
              method: 'get',
              url: 'http://103.28.32.80:8080/api/plugins/telemetry/ASSET/fe881370-9621-11eb-b183-93740c0d79ca/values/attributes?keys=alarmSetting',
              headers: { 
                  'X-Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaW5kZW5lcmd5cHJvamVjdEBleGFtcGxlLmNvbSIsInNjb3BlcyI6WyJURU5BTlRfQURNSU4iXSwidXNlcklkIjoiMDU4NWYzYjAtN2JmOS0xMWViLWI4ZGQtZWI1NWUyMWEwYjEzIiwiZmlyc3ROYW1lIjoiVXNlciBBIiwiZW5hYmxlZCI6dHJ1ZSwiaXNQdWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6ImRiZGJlN2UwLTdiZjgtMTFlYi1iOGRkLWViNTVlMjFhMGIxMyIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTYxODA0Mzg1NywiZXhwIjoxNjE4MDUyODU3fQ.y1gN5WJArefBKPn6ywqQFtKO0tD38K0UixQFFXpOLTq3mYJW9cZHYKEYay8_MrxllHbVdVvoqWVYbJs_x-zKIg'
              }
        };
        axios(config).then((response) => {
            commit('UPDATE_CURRENT_WARNING_RANGES', response.data[0].value.current.warning.ranges);
            commit('UPDATE_CURRENT_CRITICAL_RANGES', response.data[0].value.current.critical.ranges);
            commit('UPDATE_POWER_WARNING_RANGES', response.data[0].value.activePower.warning.ranges);
            commit('UPDATE_POWER_CRITICAL_RANGES', response.data[0].value.activePower.critical.ranges);
        }).catch((error) => {
            console.log(error);
        });
    },
    getComponents({ commit }) {
        var config = {
              method: 'get',
              url: 'http://103.28.32.80:8080/api/relations/info?fromId=76e83330-82e6-11eb-b183-93740c0d79ca&fromType=ASSET',
              headers: { 
                      'X-Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJtaW5kZW5lcmd5cHJvamVjdEBleGFtcGxlLmNvbSIsInNjb3BlcyI6WyJURU5BTlRfQURNSU4iXSwidXNlcklkIjoiMDU4NWYzYjAtN2JmOS0xMWViLWI4ZGQtZWI1NWUyMWEwYjEzIiwiZmlyc3ROYW1lIjoiVXNlciBBIiwiZW5hYmxlZCI6dHJ1ZSwiaXNQdWJsaWMiOmZhbHNlLCJ0ZW5hbnRJZCI6ImRiZGJlN2UwLTdiZjgtMTFlYi1iOGRkLWViNTVlMjFhMGIxMyIsImN1c3RvbWVySWQiOiIxMzgxNDAwMC0xZGQyLTExYjItODA4MC04MDgwODA4MDgwODAiLCJpc3MiOiJ0aGluZ3Nib2FyZC5pbyIsImlhdCI6MTYxODA0Mzg1NywiZXhwIjoxNjE4MDUyODU3fQ.y1gN5WJArefBKPn6ywqQFtKO0tD38K0UixQFFXpOLTq3mYJW9cZHYKEYay8_MrxllHbVdVvoqWVYbJs_x-zKIg'
                    }
        };

        axios(config)
        .then((response) => {
            var meters = [];
            var assets = [];
            response.data.forEach(elem => {
                if (elem.to.entityType === "DEVICE") {
                    meters.push({
                        name: elem.toName,
                        id: elem.to.id
                    });
                }
                if (elem.to.entityType === "ASSET") {
                    assets.push({
                        name: elem.toName,
                        id: elem.to.id
                     });
                }
            });
            commit('UPDATE_METERS', meters);
            commit('UPDATE_ASSETS', assets);
         })
        .catch(function (error) {
              console.log(error);
        });
    },
};

const getters = {
    name: state => state.name,
    additionalInfor: state => state.additionalInfor,
    method: state => state.method,
    prices: state => state.prices,
    currentWarningRanges: state => state.currentWarningRanges,
    currentCriticalRanges: state => state.currentCriticalRanges,
    powerWarningRanges: state => state.powerWarningRanges,
    powerCriticalRanges: state => state.powerCriticalRanges,
    meters: state => state.meters,
    assets: state => state.assets,
};

const store = Vuex.createStore({
    state,
    mutations,
    actions,
    getters
})


const MenuIcon = {
    template: `
        <div 
            v-for="item in items"
            v-bind:key="item.id"
            class="ui small image">
            <svg v-bind:width="item.width" v-bind:height="item.height">
                <image
                    v-bind:href="item.href"
                    x="0"
                    y="0"
                    width="100%"
                    height="100%"
                />
            </svg>
            <div
                v-if="item.error > 1"
                class="floating ui red label"
            >
                {{ item.error }}
            </div>
        </div>
    `,
    data() {
        return {
            items: [
                {
                    id: 1,
                    href: "../entity-management-asset/img/36.svg",
                    width: 41,
                    height: 27,
                    error: 0,
                },
                {
                    id: 2,
                    href: "../entity-management-asset/img/Group 78.svg",
                    width: 42,
                    height: 42,
                    error: 0,
                },
                {
                    id: 3,
                    href: "../entity-management-asset/img/Group.svg",
                    width: 50,
                    height: 50,
                    error: 0,
                },
                {
                    id: 4,
                    href: "../entity-management-asset/img/Group 77.svg",
                    width: 50,
                    height: 50,
                    error: 3,
                },
                {
                    id: 5,
                    href: "../entity-management-asset/img/Group 76.svg",
                    width: 50,
                    height: 50,
                    error: 0,
                },
                {
                    id: 6,
                    href: "../entity-management-asset/img/Group 64.svg",
                    width: 50,
                    height: 50,
                    error: 0,
                },
                {
                    id: 7,
                    href: "../entity-management-asset/img/Vector.svg",
                    width: 50,
                    height: 50,
                    error: 0,
                },
                {
                    id: 8,
                    href: "../entity-management-asset/img/Group 81.svg",
                    width: 50,
                    height: 50,
                    error: 0
                },

            ],
        }
    },
};

const GeneralInformation = {
    template: `
        <div id="general-information">
            <h3>
                General information
            </h3>
            <div>
                <div><strong>Name: </strong>{{ name }}</div>
                <div
                    v-for="(item, index) in additionalInfor"
                    v-bind:key="index"
                >
                    <strong>{{ item.key }}: </strong>{{ item.value }}
                </div>
            </div>
            <span>
                <i class="far fa-edit"></i>
                <i class="far fa-trash-alt" style="
                    color: red;
                "></i>
            </span>
        </div>
    `,
    created() {
        this.$store.dispatch('getGeneralInformation');
    },
    computed: {
        name() {
            return this.$store.getters.name;
        },
        additionalInfor() {
            return this.$store.getters.additionalInfor;
        }
    },
};

const FormulaElectric = {
    template: `
        <div id="formula-electric">
            <h3>Formula for electricity bill</h3>
            <h3 v-if="method === 'byLevel'">Calculated by level</h3>
            <h3 v-if="method === 'byTime'">Calculated by time</h3>
            <h3 v-if="method === 'fixed'">Calculated by fixed</h3>
            <div 
                v-for="(item, index) in prices"
                v-bind:key="index"
                class="formula-electric-levels">
                <div class="formula-electric-level">{{ item.name }}</div>
                <div class="formula-electric-cost">{{ item.price }}</div>
                <div class="formula-electric-unit">dong/kWh</div>
            </div>
            <span>
                <i class="far fa-edit"></i>
            </span>
        </div> 
    `,
    created() {
        this.$store.dispatch('getFormulaElectric');
    },
    computed: {
        method() {
            return this.$store.getters.method;
        },
        prices() {
            return this.$store.getters.prices;
        }
    },
};

const AlarmWhen = {
    template: `
        <div class="alarm-when">
            <div class="alarm-when-header" style="
                            margin-left: 67px;
                            margin-top: 20px;
                            font-size: 150%;
                        ">
                            <i v-bind:class="icon" v-bind:style="
                                color
                            "></i>
                            <span style="
                                font-weight: bold;
                            ">{{ nameAlarm }} when</span>
                        </div>
                        <div 
                            v-for="(range, index) in ranges"
                            v-bind:key="index"
                            data-grid="1">
                            <div>
                                <div 
                                    style="
                                    width: 121px;
                                    background-color: #fff;
                                    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                                    display: flex;
                                    align-items: center;
                                ">
                                    <span 
                                        style="
                                        padding: 3px 24px 3px 24px;
                                        min-width: 70px;
                                    ">{{ range.lower.value }}</span>
                                    <span style="
                                        padding: 3px;
                                        padding-left: 10px;
                                        border-right: 7px solid #4DC367;
                                        background-color: #4DC367;
                                        color: #fff;
                                        min-width: 51px;
                                    ">{{ range.lower.is }}</span>
                                </div>
                            </div>
                            <div>value</div>
                            <div>
                                <div style="
                                    width: 121px;
                                    background-color: #fff;
                                    box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.25);
                                    display: flex;
                                    align-items: center;
                                ">
                                    <span style="
                                        padding: 3px;
                                        padding-left: 10px;
                                        border-right: 7px solid #4DC367;
                                        background-color: #4DC367;
                                        color: #fff;
                                        min-width: 51px;
                                    ">{{ range.upper.is }}</span>
                                    <span style="
                                        padding: 3px 24px 3px 24px;
                                        min-width: 70px;
                                    ">{{ range.upper.value }}</span>
                                </div>
                            </div>
                        </div>
                        <button class="positive ui button" style="
                            margin-top: 1em;
                            margin-left: 67px;
                            border-radius: 40px;
                            padding: 0.7em;
                            padding-right: 1em;
                            padding-left: 1em;
                            font-size: 90%;
                        ">
                            OR
                        </button>
                    </div> 
    `,
    data() {
        return {
        };
    },
    props: [ "nameAlarm", "icon", "color", "ranges" ],
};

const AlarmSetting = {
    template: `
        <div id="alarm-setting">
            <h3>Alarm setting</h3>
            <div class="ui top attached tabular menu" style="
                border: 0px;
            ">
                <a 
                    v-on:click="onChangeAlarm"
                    v-bind:class="activeItemFirst" 
                    name="current"
                    data-tab="first" style="
                ">
                    Current
                </a>
                <a 
                    v-on:click="onChangeAlarm"
                    v-bind:class="activeItemSecond" 
                    name="activePower"
                    data-tab="second"
                    style="
                    ">
                    Active Power
                </a>
            </div>
            <div 
                v-bind:class="activeTabFirst"
                data-tab="first" style="
                    border: 0px;
                "
            >
                <!-- Warning when -->
                <alarm-when 
                    class="alarm-when-first" 
                    v-bind:nameAlarm="'Warning'"
                    v-bind:icon="'fas fa-bell'"
                    v-bind:color="'color: #FFC107;margin-right: 0.5em'"
                    v-bind:ranges="currentWarningRanges"
                ></alarm-when>
                <alarm-when 
                    class="alarm-when-second" 
                    v-bind:nameAlarm="'Critical'"
                    v-bind:icon="'fas fa-exclamation-triangle'"
                    v-bind:color="'color: #E10000;margin-right: 0.5em'"
                    v-bind:ranges="currentCriticalRanges"
                ></alarm-when>
                <!-- Critical when -->
            </div>
            <div
                v-bind:class="activeTabSecond"
                data-tab="second"
                style="
                    border: 0px;
                "
            >
                <!-- Warning when -->
                <alarm-when 
                    class="alarm-when-first" 
                    v-bind:nameAlarm="'Warning'"
                    v-bind:icon="'fas fa-bell'"
                    v-bind:color="'color: #FFC107;margin-right: 0.5em'"
                    v-bind:ranges="powerWarningRanges"
                ></alarm-when>
                <alarm-when 
                    class="alarm-when-second" 
                    v-bind:nameAlarm="'Critical'"
                    v-bind:icon="'fas fa-exclamation-triangle'"
                    v-bind:color="'color: #E10000;margin-right: 0.5em'"
                    v-bind:ranges="powerCriticalRanges"
                ></alarm-when>
                <!-- Critical when -->

            </div>
        </div>
    `,
    components: {
        "alarm-when": AlarmWhen,
    },
    data() {
        return {
            activeItemFirst: 'active item',
            activeItemSecond: 'item',
            activeTabFirst: 'ui bottom attached active tab segment',
            activeTabSecond: 'ui bottom attached tab segment',
            ranges: [
                {
                    lower: {
                        is: ">=",
                        value: -10
                    },
                    upper: {
                        is: "<",
                        value: -5
                    }
                },
                {
                    lower: {
                        is: ">",
                        value: 5
                    },
                    upper: {
                        is: "<=",
                        value: 10
                    },
                },
            ],
        };
    },
    methods: {
        onChangeAlarm(evt) {
            console.log(evt.target.name);
            if (evt.target.name === 'current') {
                this.activeItemFirst = 'active item';
                this.activeItemSecond = 'item';
                this.activeTabFirst = 'ui bottom attached active tab segment';
                this.activeTabSecond = 'ui bottom attached tab segment';
            } else {
                this.activeItemFirst = 'item';
                this.activeItemSecond = 'active item';
                this.activeTabFirst = 'ui bottom attached tab segment';
                this.activeTabSecond = 'ui bottom attached active tab segment';
            }
        },
    },
    created() {
        this.$store.dispatch('getAlarmSetting');
    },
    computed: {
        currentWarningRanges() {
            return this.$store.getters.currentWarningRanges;
        },
        currentCriticalRanges() {
            return this.$store.getters.currentCriticalRanges;
        },
        powerWarningRanges() {
            return this.$store.getters.powerWarningRanges;
        },
        powerCriticalRanges() {
            return this.$store.getters.powerCriticalRanges;
        },
    },
};

const ComponentsManagement = {
    template: `
        <div id="components">
            <h3>Components</h3>
            <div class="ui list">
                <!-- 1 -->
                <div class="item">
                    <i class="info icon"></i>
                    <div class="content">
                        <div class="header">Meter</div>
                        <div class="list">
                            <div 
                                v-for="(meter, index) in meters"
                                v-bind:key="index"
                                class="item">
                                <i class="tachometer alternate icon"></i>
                                <div class="content">
                                    <div class="header">{{ meter.name }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--- 2 -->
                <div class="item">
                    <i class="plus square outline icon"></i>
                    <div class="content">
                        <div class="header">Sub asset</div>
                        <div class="list">
                            <div 
                                v-for="(asset, index) in assets"
                                v-bind:key="index"
                                class="item">
                                <i class="sitemap icon"></i>
                                <div class="content">
                                    <div class="header">{{ asset.name }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    `,
    data() {
        return {
        };
    },
    created() {
        this.$store.dispatch('getComponents');
    },
    computed: {
        meters() {
            return this.$store.getters.meters;
        },
        assets() {
            return this.$store.getters.assets;
        },
    },
};

const app = Vue.createApp({
    components: {
        "menu-icon": MenuIcon,
        "formula-electric": FormulaElectric,
        "general-information": GeneralInformation,
        "alarm-setting": AlarmSetting,
        "components-management": ComponentsManagement,
    },
})

app.use(store)
app.mount("#app");
