const MenuIcon = {
    template: `
        <div 
            v-for="item in items"
            v-bind:key="item.id"
            class="ui small image">
            <svg v-bind:width="item.width" v-bind:height="item.height">
                <image
                    v-bind:href="item.href"
                    x="0"
                    y="0"
                    width="100%"
                    height="100%"
                />
            </svg>
            <div
                v-if="item.error > 1"
                class="floating ui red label"
            >
                {{ item.error }}
            </div>
        </div>
    `,
    data() {
        return {
            items: [
                {
                    id: 1,
                    href: "../entity-management-asset/img/36.svg",
                    width: 41,
                    height: 27,
                    error: 0,
                },
                {
                    id: 2,
                    href: "../entity-management-asset/img/Group 78.svg",
                    width: 42,
                    height: 42,
                    error: 0,
                },
                {
                    id: 3,
                    href: "../entity-management-asset/img/Group.svg",
                    width: 50,
                    height: 50,
                    error: 0,
                },
                {
                    id: 4,
                    href: "../entity-management-asset/img/Group 77.svg",
                    width: 50,
                    height: 50,
                    error: 3,
                },
                {
                    id: 5,
                    href: "../entity-management-asset/img/Group 76.svg",
                    width: 50,
                    height: 50,
                    error: 0,
                },
                {
                    id: 6,
                    href: "../entity-management-asset/img/Group 64.svg",
                    width: 50,
                    height: 50,
                    error: 0,
                },
                {
                    id: 7,
                    href: "../entity-management-asset/img/Vector.svg",
                    width: 50,
                    height: 50,
                    error: 0,
                },
                {
                    id: 8,
                    href: "../entity-management-asset/img/Group 81.svg",
                    width: 50,
                    height: 50,
                    error: 0
                },

            ],
        }
    },
};

const CostDashboard = {
    template: `
        <div id="cost">
            <div class="cost-header" style="
                color: #FFB926;
            ">
                <i class="fas fa-file-invoice-dollar" style="
                    margin-right: 0.5em;
                "></i>
                <span>Cost</span>
            </div>
            <div class="details">
                <div 
                    v-for="item in items"
                    v-bind:key="item.id"
                    class="detail">
                    {{ item.title }}
                    <span v-if="item.status" style="
                        color: #D11616;
                    ">
                        <i
                            class="fas fa-angle-double-up"></i>
                        {{ item.rate }}
                    </span>
                    <span v-if="!item.status" style="
                        color: #4FBA6F;
                    ">
                        <i 
                            class="fas fa-angle-double-down"></i>
                        {{ item.rate }}
                    </span>
                    <p>$ {{ item.cost }}</p>
                </div>
            </div>
            <div class="this-month-cost">
                <div id="title" class="ui breadcrumb">
                    <div class="active section">This month</div>
                </div>
                <div id="bar-chart">
                    <canvas ref="chart" style=" 
                        height: 201px;
                        width: 821px;
                    "></canvas>
                </div>
                <div id="pie-chart">
                    <canvas ref="chart1" style="
                        height: 217px;
                        width: 217px;
                    "></canvas>
                </div>
            </div>
        </div>
    `,
    data() {
        return {
            items: [
                {
                    id: 1,
                    title: 'This year',
                    status: false,
                    rate: 15,
                    cost: 28770
                },
                {
                    id: 2,
                    title: 'This month',
                    status: false,
                    rate: 15,
                    cost: 1451
                },
                {
                    id: 3,
                    title: 'Today',
                    status: true,
                    rate: 1,
                    cost: 34 
                },
            ],
        }
    },
    mounted() {
        var myfield = {
            labels: ["01-Jan","02-Jan","03-Jan","04-Jan","05-Jan","06-Jan","07-Jan","08-Jan","09-Jan","10-Jan","11-Jan","12-Jan","13-Jan","14-Jan","15-Jan","16-Jan","17-Jan","18-Jan","19-Jan","20-Jan","21-Jan","22-Jan","23-Jan","24-Jan","25-Jan","26-Jan","27-Jan","28-Jan","29-Jan","30-Jan","31-Jan"],
            datasets: [
            {
                label: "Peak",
                backgroundColor: pattern.draw('square', '#ff6384'),
                barPercentage: 0.5,
                data: [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10],
            },
            {
                label: "Normal",
                backgroundColor: pattern.draw('circle', '#36a2eb'),
                barPercentage: 0.5,
                data: [20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20],
            },
            {
                label: "Off-Peak",
                backgroundColor: pattern.draw('square', '#cc65fe'),
                barPercentage: 0.5,
                data: [30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30],
            },
            ],
        };
        var myfield2 = {
            labels: ["Peak", "Normal", "Off-Peak"],
            datasets: [{
                backgroundColor: [
                    pattern.draw('square', '#ff6384'),
                    pattern.draw('circle', '#36a2eb'),
                    pattern.draw('square', '#cc65fe'),
                ],
                data: [19, 74, 7],
            }],
        };
        var chart = this.$refs.chart;
        var ctx = chart.getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'bar',
            options: {
                plugins: {
                    title: {
                        display: false,
                        text: 'This month',
                    },
                    legend: {
                        display: false,
                    },
                },
                scales: {
                    x: {
                        stacked: true,
                    },
                    y: {
                        stacked: true
                    }
                },
                maintainAspecRatio: false,
            },
            data: myfield,
        });
        chart = this.$refs.chart1;
        ctx = chart.getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'pie',
            options: {
                plugins: {
                    legend: {
                        position: "bottom",
                        labels: {
                            boxWidth: 20,
                        }
                    },
                },
                maintainAspecRatio: false,
            },
            data: myfield2,
        });
    },
};

const ConsumptionDashboard = {
    template: `
        <div id="consumption">
            <div class="cost-header" style="
                color: #26A2FC;
            ">
                <i class="fas fa-file-invoice-dollar" style="
                    margin-right: 0.5em;
                "></i>
                <span>Cost</span>
            </div>
            <div class="details">
                <div 
                    v-for="item in items"
                    v-bind:key="item.id"
                    class="detail">
                    {{ item.title }}
                    <span v-if="item.status" style="
                        color: #D11616;
                    ">
                        <i
                            class="fas fa-angle-double-up"></i>
                        {{ item.rate }}
                    </span>
                    <span v-if="!item.status" style="
                        color: #4FBA6F;
                    ">
                        <i 
                            class="fas fa-angle-double-down"></i>
                        {{ item.rate }}
                    </span>
                    <p>{{ item.cost }} kWh</p>
                </div>
            </div>
            <div class="this-month-cost">
                <div id="title" class="ui breadcrumb">
                    <div class="active section">This month</div>
                    <div class="divider"> | </div>
                    <div class="section">Today</div>
                </div>
                <div id="bar-chart">
                    <canvas ref="chart" style="
                        height: 201px;
                        width: 821px;
                    "></canvas>
                </div>
                <div id="pie-chart">
                    <canvas ref="chart1" style="
                        height: 217px;
                        width: 217px;
                    "></canvas>
                </div>
            </div>
            <div class="this-month-lengend">
                <table class="ui table" style="
                    font-size: 70%;
                ">
                    <thead>
                        <tr>
                            <th class="ten wide"></th>
                            <th>min</th>
                            <th>average</th>
                            <th>max</th>
                            <th>total</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-label="room">
                                <i class="fas fa-square" style="
                                    color: #ff6384;
                                "></i>
                                Room 201
                            </td>
                            <td data-label="min">23</td>
                            <td data-label="average">23</td>
                            <td data-label="max">123</td>
                            <td data-label="total">123</td>
                        </tr>
                        <tr>
                            <td data-label="room">
                                <i class="fas fa-square" style="
                                    color: #ff6384;
                                "></i>
                                Room 201
                            </td>
                            <td data-label="min">23</td>
                            <td data-label="average">23</td>
                            <td data-label="max">123</td>
                            <td data-label="total">123</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    `,
    data() {
        return {
            items: [
                {
                    id: 1,
                    title: 'This year',
                    status: false,
                    rate: 15,
                    cost: 28770
                },
                {
                    id: 2,
                    title: 'This month',
                    status: false,
                    rate: 15,
                    cost: 1451
                },
                {
                    id: 3,
                    title: 'Today',
                    status: true,
                    rate: 1,
                    cost: 34 
                },
            ],
        }
    },
    mounted() {
        var myfield = {
            labels: ["01-Jan","02-Jan","03-Jan","04-Jan","05-Jan","06-Jan","07-Jan","08-Jan","09-Jan","10-Jan","11-Jan","12-Jan","13-Jan","14-Jan","15-Jan","16-Jan","17-Jan","18-Jan","19-Jan","20-Jan","21-Jan","22-Jan","23-Jan","24-Jan","25-Jan","26-Jan","27-Jan","28-Jan","29-Jan","30-Jan","31-Jan"],
            datasets: [
            {
                label: "Peak",
                backgroundColor: pattern.draw('square', '#ff6384'),
                barPercentage: 0.5,
                data: [10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10,10],
            },
            {
                label: "Normal",
                backgroundColor: pattern.draw('circle', '#36a2eb'),
                barPercentage: 0.5,
                data: [20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20,20],
            },
            {
                label: "Off-Peak",
                backgroundColor: pattern.draw('square', '#cc65fe'),
                barPercentage: 0.5,
                data: [30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30,30],
            },
            ],
        };
        var myfield2 = {
            labels: ["Peak", "Normal", "Off-Peak"],
            datasets: [{
                backgroundColor: [
                    pattern.draw('square', '#ff6384'),
                    pattern.draw('circle', '#36a2eb'),
                    pattern.draw('square', '#cc65fe'),
                ],
                data: [19, 74, 7],
            }],
        };
        var chart = this.$refs.chart;
        var ctx = chart.getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'bar',
            options: {
                plugins: {
                    title: {
                        display: false,
                        text: 'This month',
                    },
                    legend: {
                        display: false,
                    },
                },
                scales: {
                    x: {
                        stacked: true,
                    },
                    y: {
                        stacked: true
                    }
                },
                maintainAspecRatio: false,
            },
            data: myfield,
        });
        chart = this.$refs.chart1;
        ctx = chart.getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'pie',
            options: {
                plugins: {
                    legend: {
                        position: "bottom",
                        labels: {
                            boxWidth: 20,
                        }
                    },
                },
                maintainAspecRatio: false,
            },
            data: myfield2,
        });
    },
};

const Panel = {
    template: `
        <div v-bind:id="id">
            {{ title }}
            <span>{{ value }} V</span>
        </div>
    `,
    props: ["title", "id", "value" ],
};

var arr = [];
while(arr.length < 30) {
    var r = Math.floor(Math.random() * 100) + 1;
    if(arr.indexOf(r) === -1) arr.push(r);
};

const LoadCurve = {
    template: `
        <div id="load-curve">
            <div class="header-dashboard">
                Load curve
                <span>(today)</span>
            </div>
            <div id="line-chart">
                <canvas ref="chart" style="
                    width: 100%;
                    height: 100%;
                ">
                </canvas>
            </div>
        </div>
    `,
    mounted() {
        var myfield = {
            labels: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10,
                     11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
                     21, 22, 23, 24, 25, 26, 27, 28, 29, 30],
            datasets: [{
                data: arr, 
                borderColor: '#6B8FD0',
                backgroundColor: '#6B8FD0',
            }],
        };
        var chart = this.$refs.chart;
        var ctx = chart.getContext("2d");
        var myChart = new Chart(ctx, {
            type: 'line',
            options:  {
                plugins: {
                    title: {
                        display: true,
                        text: 'Active power'
                    },
                    legend: {
                        display: false,
                    }
                },
                maintainAspecRatio: false,
                // responsive: false
            },
            data: myfield
        });
    },
};

const AlarmDashboard = {
    template: `
        <div id="alarm">
            <div class="header-dashboard">
                Alarm
                <span>(today)</span>
            </div>
            <div class="alarm-active">
                <div class="alarm-origin" style="
                    color: #2394E7;
                ">
                    Active
                    <p style="
                        font-size: 150%;
                    ">7</p>
                </div>
                <div class="alarm-critical" style="
                    color: #D11616;
                    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
                    padding: 0.5em;
                    padding-top: 0px;
                    padding-bottom: 30px;
                    margin-right: 10px;
                ">
                    critical
                    <p style="
                        margin-top: 12px;
                    ">0</p>
                </div>
                <div class="alarm-warning" style="
                    color: #FFB926;
                    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.25);
                    padding: 0.5em;
                    padding-top: 0px;
                    padding-bottom: 30px;
                    margin-right: 10px;
                ">
                    warning
                    <p style="
                        margin-top: 12px;
                    ">7</p>
                </div>
                <div class="alarm-dashboard">
                    <canvas ref="chart"></canvas>
                </div>
            </div>
            <div class="unack">
                <div
                    v-for="item in items"
                    v-bind:key="item.id"
                >
                    {{ item.title }}
                    <p style="
                        margin-top: 10px;
                    ">{{ item.value }}</p>
                </div>
            </div>
            <div class="alarm-table">
                <table class="ui table" style="
                    font-size: 60%;
                ">
                    <thead>
                        <tr style="
                            background-color: #fff;
                        ">
                            <th></th>
                            <th class="three wide">Created time</th>
                            <th class="six wide">Originator</th>
                            <th class="three wide">Type</th>
                            <th>Serverity</th>
                            <th class="three wide"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td data-label="isChecked">
                                <input
                                    v-model="isChecked"
                                    type="checkbox"
                                />
                            </td>
                            <td>2021-01-01 00:00:03</td>
                            <td>Room 201</td>
                            <td>Voltage</td>
                            <td style="
                                color: #F6CB70;
                            ">Warning</td>
                            <td class="right aligned">
                                <i class="fas fa-ellipsis-h" style="
                                    margin-right: 16px;
                                "></i>
                                <i class="fas fa-check" style="
                                    margin-right: 16px;
                                "></i>
                                <i class="fas fa-times"></i>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div class="data-footer">
                    <div class="data-footer__pagination" style="
                        margin-right: 10px;
                    ">1-5 of 10</div>
                    <div class="data-footer__icons-before">
                        <button
                            type="button"
                            v-bind:disabled="isDisabled"
                            aria-label="Previous page"
                            class="circular ui icon button"
                        >
                            <i class="icon chevron left"></i>
                        </button>
                    </div>
                    <div class="data-footer__icons-after">
                        <button
                            type="button"
                            aria-label="Next page"
                            class="circular ui icon button"
                        >
                            <i class="icon chevron right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    `,
    data() {
        return {
            isChecked: false,
            items: [
                {
                    id: 1,
                    title: 'Unacknowledged',
                    value: 2
                },
                {
                    id: 2,
                    title: 'critical',
                    value: 0
                },
                {
                    id: 3,
                    title: 'warning',
                    value: 0
                },
            ],
        }
    },
    mounted() {
        var myfield = {
            labels: ['parameter', 'sensor'],
            datasets: [{
                data: [6, 1],
                backgroundColor: [
                    '#70AD47',
                    '#4472C4'
                ]
            }],
        };
        var chart = this.$refs.chart;
        var ctx = chart.getContext('2d');
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            options: {
                plugins: {
                    legend: {
                        display: true,
                        position: 'right',
                        labels: {
                            boxWidth: 10,
                            boxHeight: 10,
                        }
                    },
                },
                maintainAspecRatio: false,
            },
            data: myfield,
        });
    },
};

Vue.createApp({
    components: {
        "menu-icon": MenuIcon,
        "cost-dashboard": CostDashboard,
        "consumption-dashboard": ConsumptionDashboard,
        "panel": Panel,
        "load-curve": LoadCurve,
        "alarm-dashboard": AlarmDashboard,
    },
}).mount("#app");
